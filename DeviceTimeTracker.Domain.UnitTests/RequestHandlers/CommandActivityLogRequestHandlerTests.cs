using System;
using System.Threading.Tasks;
using AutoMapper;
using DeviceTimeTracker.Domain.Models;
using DeviceTimeTracker.Domain.RequestHandlers;
using DeviceTimeTracker.Domain.Requests.ActivityLog;
using DeviceTimeTracker.Domain.Services.Abstract;
using Moq;
using NUnit.Framework;

namespace DeviceTimeTracker.Domain.UnitTests.RequestHandlers
{
    [TestFixture]
    public class CommandActivityLogRequestHandlerTests
    {
        private CommandActivityLogRequestHandler target;

        private Mock<IActivityLogService> activityLogServiceMock;
        private Mock<IMapper> mapperMock;

        [SetUp]
        public void SetUp()
        {
            activityLogServiceMock = new Mock<IActivityLogService>();
            mapperMock = new Mock<IMapper>();

            target = new CommandActivityLogRequestHandler(activityLogServiceMock.Object, mapperMock.Object);
        }

        [Test]
        public void PostActivity_WhenNull_Throws()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => target.PostActivity(null));
        }

        [Test]
        public async Task PostActivity_Calls_Service()
        {
            var request = new PostActivityLogRequest();
            _ = await target.PostActivity(request);

            activityLogServiceMock.Verify(x => x.CreateAsync(It.IsAny<ActivityLog>()), Times.Once);
            activityLogServiceMock.VerifyNoOtherCalls();
        }
    }
}
