﻿using System;

namespace DeviceTimeTracker.Api.Models
{
    public class ReadActivityLogModelDetails
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Workstation { get; set; }
        public DateTime StartTimestamp { get; set; }
        public DateTime EndTimestamp { get; set; }
        public int Duration { get; set; }
    }
}
