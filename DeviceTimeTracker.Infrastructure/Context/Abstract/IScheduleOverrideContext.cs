﻿using DeviceTimeTracker.Domain.Models;
using MongoDB.Driver;

namespace DeviceTimeTracker.Infrastructure.Context.Abstract
{
    public interface IScheduleOverrideContext : IContext
    {
        IMongoCollection<ScheduleOverride> ScheduleOverrideCollection { get; }
    }
}