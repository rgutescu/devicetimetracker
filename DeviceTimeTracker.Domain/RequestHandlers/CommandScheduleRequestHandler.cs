﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DeviceTimeTracker.Domain.Exceptions;
using DeviceTimeTracker.Domain.RequestHandlers.Abstract;
using DeviceTimeTracker.Domain.Requests.Schedule;
using DeviceTimeTracker.Domain.Responses.Schedule;
using DeviceTimeTracker.Domain.Services.Abstract;

namespace DeviceTimeTracker.Domain.RequestHandlers
{
    public class CommandScheduleRequestHandler : ICommandScheduleRequestHandler
    {
        private readonly IScheduleService scheduleService;
        private readonly IMapper mapper;

        public CommandScheduleRequestHandler(
            IScheduleService scheduleService,
            IMapper mapper)
        {
            this.scheduleService = scheduleService;
            this.mapper = mapper;
        }

        public async Task<ScheduleResponse> PutSchedule(PutScheduleRequest request)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            if (!IsValidSchedule(request, out var validationMessage))
            {
                throw new InvalidModelException(validationMessage);
            }

            var schedule = mapper.Map<Models.Schedule>(request);

            var result = await scheduleService.SetAsync(schedule);

            return mapper.Map<ScheduleResponse>(result);
        }

        private bool IsValidSchedule(PutScheduleRequest request, out string validationMessage)
        {
            validationMessage = null;

            if (request.Intervals?.Length != 7)
            {
                validationMessage = "Schedule intervals are required for all 7 days of the week";
                return false;
            }

            var distinctDays = request.Intervals.Select(s => s.Day).Distinct().ToList();
            if (distinctDays.Count != 7)
            {
                validationMessage = "Distinct days for schedule intervals are required";
                
                return false;
            }

            foreach (var interval in request.Intervals)
            {
                if (interval.Start > interval.End)
                {
                    validationMessage = $"Date range interval for day {interval.Day} is not valid. Start hour should be before End hour";

                    return false;
                }
            }

            return true;
        }
    }
}
