﻿namespace DeviceTimeTracker.Domain
{
    public static class Constants
    {
        public static class Search
        {
            public const int ActivitySearchMaxDaysInterval = 30;
        }

        public static class Regexp
        {
            public const string HourRegexPattern = "^[012][0-9]:[012345][0-9]$";
            public const string DayRegexPattern = "^[1-7]$";
        }
    }
}
