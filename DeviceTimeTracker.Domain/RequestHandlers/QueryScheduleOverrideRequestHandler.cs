﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using DeviceTimeTracker.Domain.RequestHandlers.Abstract;
using DeviceTimeTracker.Domain.Requests.ScheduleOverride;
using DeviceTimeTracker.Domain.Responses.ScheduleOverride;
using DeviceTimeTracker.Domain.Services.Abstract;

namespace DeviceTimeTracker.Domain.RequestHandlers
{
    public class QueryScheduleOverrideRequestHandler : IQueryScheduleOverrideRequestHandler
    {
        private readonly IScheduleOverrideService scheduleOverrideService;
        private readonly IMapper mapper;

        public QueryScheduleOverrideRequestHandler(
            IScheduleOverrideService scheduleOverrideService,
            IMapper mapper)
        {
            this.scheduleOverrideService = scheduleOverrideService;
            this.mapper = mapper;
        }

        public async Task<ScheduleOverrideResponse> GetScheduleOverrideByUserId(GetScheduleOverrideRequest request)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var result = await scheduleOverrideService.GetByUserIdAsync(request.UserId, request.Date);

            return mapper.Map<ScheduleOverrideResponse>(result);
        }
    }
}
