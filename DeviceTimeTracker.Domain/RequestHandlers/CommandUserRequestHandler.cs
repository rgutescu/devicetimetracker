﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using DeviceTimeTracker.Domain.Exceptions;
using DeviceTimeTracker.Domain.RequestHandlers.Abstract;
using DeviceTimeTracker.Domain.Requests.User;
using DeviceTimeTracker.Domain.Responses.User;
using DeviceTimeTracker.Domain.Services.Abstract;

namespace DeviceTimeTracker.Domain.RequestHandlers
{
    public class CommandUserRequestHandler : ICommandUserRequestHandler
    {
        private readonly IUserService userService;
        private readonly IMapper mapper;

        public CommandUserRequestHandler(
            IUserService userService,
            IMapper mapper)
        {
            this.userService = userService;
            this.mapper = mapper;
        }

        public async Task<UserResponse> PostUser(PostUserRequest request)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var user = mapper.Map<Models.User>(request);

            var result = await userService.CreateAsync(user);

            return mapper.Map<UserResponse>(result);
        }

        public async Task<UserResponse> PutUser(PutUserRequest request)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var existingUser = await userService.GetByIdAsync(request.Id);
            if (existingUser == null)
            {
                throw new EntityNotFoundException($"User with id {request.Id} is not present in the system");
            }

            var user = mapper.Map<Models.User>(request);

            var result = await userService.UpdateAsync(user);

            return mapper.Map<UserResponse>(result);
        }
    }
}
