﻿using System;
using System.Threading.Tasks;
using DeviceTimeTracker.Domain.Models;
using DeviceTimeTracker.Domain.Repositories;
using DeviceTimeTracker.Domain.Services.Abstract;

namespace DeviceTimeTracker.Infrastructure.Services
{
    public class ScheduleOverrideService : IScheduleOverrideService
    {
        private readonly IScheduleOverrideRepository scheduleOverrideRepository;

        public ScheduleOverrideService(IScheduleOverrideRepository scheduleOverrideRepository)
        {
            this.scheduleOverrideRepository = scheduleOverrideRepository;
        }

        public Task<ScheduleOverride> GetByUserIdAsync(string userId, DateTime date)
        {
            return scheduleOverrideRepository.GetByUserIdAsync(userId, date);
        }

        public async Task<ScheduleOverride> SetAsync(ScheduleOverride scheduleOverride)
        {
            var existingUserScheduleOverride = await scheduleOverrideRepository.GetByUserIdAsync(scheduleOverride.UserId, scheduleOverride.Date);

            if (existingUserScheduleOverride == null)
            {
                return await scheduleOverrideRepository.CreateAsync(scheduleOverride);
            }

            scheduleOverride.Id = existingUserScheduleOverride.Id;
            return await scheduleOverrideRepository.UpdateAsync(scheduleOverride);
        }
    }
}