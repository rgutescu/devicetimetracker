﻿using Microsoft.AspNetCore.Builder;

namespace DeviceTimeTracker.Api.Middleware
{
    public static class MiddlewareExtension
    {
        public static void UseCustomExceptionMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionMiddleware>();
        }
    }
}
