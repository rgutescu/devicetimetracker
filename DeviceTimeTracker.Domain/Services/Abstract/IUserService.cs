﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DeviceTimeTracker.Domain.Models;

namespace DeviceTimeTracker.Domain.Services.Abstract
{
    public interface IUserService
    {
        Task<IEnumerable<User>> GetAsync();
        Task<User> GetByIdAsync(string id);
        Task<User> CreateAsync(User user);
        Task<User> UpdateAsync(User user);
    }
}