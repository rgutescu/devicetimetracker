﻿using System.ComponentModel.DataAnnotations;
using DeviceTimeTracker.Domain;

namespace DeviceTimeTracker.Api.Models
{
    public class ScheduleIntervalRawModel
    {
        
        [RegularExpression(Constants.Regexp.DayRegexPattern, ErrorMessage = "Day should be a numerical value between 1 and 7")]
        public int Day { get; set; }

        [RegularExpression(Constants.Regexp.HourRegexPattern, ErrorMessage = "Format: hh:mm")]
        public string StartTime { get; set; }

        [RegularExpression(Constants.Regexp.HourRegexPattern, ErrorMessage = "Format: hh:mm")]
        public string EndTime { get; set; }

        public int AllowedMinutes { get; set; }
    }
}
