﻿using MongoDB.Driver;

namespace DeviceTimeTracker.Infrastructure.Context.Abstract
{
    public interface IContext
    {
        IMongoDatabase Database { get; }
    }
}