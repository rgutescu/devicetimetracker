﻿using System;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;

namespace DeviceTimeTracker.Domain.Models
{
    public class Schedule
    {
        [BsonId(IdGenerator = typeof(StringObjectIdGenerator))]
        public string Id { get; set; }
        public string UserId { get; set; }
        public ScheduleIntervalModel[] Intervals { get; set; }

        public static Schedule Default(string userId)
        {
            var intervals = new ScheduleIntervalModel[7];
            for (var i = 0; i < 7; i++)
            {
                intervals[i] = new ScheduleIntervalModel
                {
                    Day = i + 1,
                    Start = new TimeSpan(8, 0, 0),
                    End = new TimeSpan(22, 0, 0),
                    AllowedMinutes = 4 * 60
                };
            }

            return new Schedule
            {
                UserId = userId,
                Intervals = intervals
            };
        }
    }
}
