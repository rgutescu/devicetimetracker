using DeviceTimeTracker.Domain.Repositories;
using DeviceTimeTracker.Infrastructure.Services;
using Moq;
using NUnit.Framework;
using System.Threading.Tasks;
using DeviceTimeTracker.Domain.Models;

namespace DeviceTimeTracker.Infrastructure.UnitTests.Services
{
    [TestFixture]
    public class UserServiceTests
    {
        private UserService target;

        private Mock<IUserRepository> userRepositoryMock;
        private Mock<IScheduleRepository> scheduleRepositoryMock;

        [SetUp]
        public void SetUp()
        {
            userRepositoryMock = new Mock<IUserRepository>();
            scheduleRepositoryMock = new Mock<IScheduleRepository>();

            target = new UserService(userRepositoryMock.Object, scheduleRepositoryMock.Object);
        }

        [Test]
        public async Task GetAsync_Calls_Repository()
        {
            _ = await target.GetAsync();

            userRepositoryMock.Verify(x => x.GetUsersAsync(), Times.Once);
            userRepositoryMock.VerifyNoOtherCalls();
        }

        [Test]
        public async Task GetByIdAsync_Calls_Repository()
        {
            var userId = "1";
            _ = await target.GetByIdAsync(userId);

            userRepositoryMock.Verify(x => x.GetByIdAsync(userId), Times.Once);
            userRepositoryMock.VerifyNoOtherCalls();
        }

        [Test]
        public async Task CreateAsync_Calls_Repositories()
        {
            var userName = "username";
            var userId = "1";
            var user = new User {UserName = userName, Id = userId};

            userRepositoryMock.Setup(x => x.CreateAsync(user)).ReturnsAsync(user);

            var createdUser = await target.CreateAsync(user);

            userRepositoryMock.Verify(x => x.CreateAsync(user), Times.Once);
            userRepositoryMock.VerifyNoOtherCalls();

            scheduleRepositoryMock.Verify(x => x.CreateAsync(It.Is<Schedule>(s => s.UserId == createdUser.Id)), Times.Once);
            scheduleRepositoryMock.VerifyNoOtherCalls();
        }

        [Test]
        public async Task UpdateAsync_Calls_Repository()
        {
            var user = new User {UserName = "abc"};
            _ = await target.UpdateAsync(user);

            userRepositoryMock.Verify(x => x.UpdateAsync(user), Times.Once);
            userRepositoryMock.VerifyNoOtherCalls();
        }
    }
}
