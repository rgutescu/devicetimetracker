using System;
using System.Threading.Tasks;
using AutoMapper;
using DeviceTimeTracker.Domain.RequestHandlers;
using DeviceTimeTracker.Domain.Requests.Schedule;
using DeviceTimeTracker.Domain.Services.Abstract;
using Moq;
using NUnit.Framework;

namespace DeviceTimeTracker.Domain.UnitTests.RequestHandlers
{
    [TestFixture]
    public class QueryScheduleRequestHandlerTests
    {
        private QueryScheduleRequestHandler target;

        private Mock<IScheduleService> scheduleServiceMock;
        private Mock<IScheduleReporterService> scheduleReporterServiceMock;
        private Mock<IMapper> mapperMock;

        [SetUp]
        public void SetUp()
        {
            scheduleServiceMock = new Mock<IScheduleService>();
            scheduleReporterServiceMock = new Mock<IScheduleReporterService>();
            mapperMock = new Mock<IMapper>();

            target = new QueryScheduleRequestHandler(scheduleServiceMock.Object, scheduleReporterServiceMock.Object, mapperMock.Object);
        }

        [Test]
        public void GetScheduleByUserId_WhenNull_Throws()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => target.GetScheduleByUserId(null));
        }

        [Test]
        public async Task GetScheduleByUserId_Calls_Service()
        {
            var userId = "112";
            var request = new GetScheduleRequest { UserId = userId };
            _ = await target.GetScheduleByUserId(request);

            scheduleServiceMock.Verify(x => x.GetByUserIdAsync(userId), Times.Once);
            scheduleServiceMock.VerifyNoOtherCalls();
        }

        [Test]
        public void GetUserScheduleCurrentStatus_WhenNull_Throws()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => target.GetUserScheduleCurrentStatus(null));
        }

        [Test]
        public async Task GetUserScheduleCurrentStatus_Calls_Service()
        {
            var userId = "112";
            var request = new GetScheduleStatusRequest { UserId = userId };
            _ = await target.GetUserScheduleCurrentStatus(request);

            scheduleReporterServiceMock.Verify(x => x.GetUserScheduleCurrentStatus(userId), Times.Once);
            scheduleReporterServiceMock.VerifyNoOtherCalls();
        }
    }
}
