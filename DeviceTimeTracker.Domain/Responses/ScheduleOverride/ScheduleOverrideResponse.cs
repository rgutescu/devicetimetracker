﻿using System;
using DeviceTimeTracker.Domain.Requests.ScheduleOverride;

namespace DeviceTimeTracker.Domain.Responses.ScheduleOverride
{
    public class ScheduleOverrideResponse
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public DateTime Date { get; set; }
        public ScheduleOverrideIntervalDto Interval { get; set; }
    }
}
