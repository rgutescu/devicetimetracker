﻿using AutoMapper;
using DeviceTimeTracker.Domain.Models;
using DeviceTimeTracker.Domain.Requests.User;
using DeviceTimeTracker.Domain.Responses.User;

namespace DeviceTimeTracker.Domain.Mapper
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserResponse>();
            CreateMap<PostUserRequest, User>();
            CreateMap<PutUserRequest, User>();
            CreateMap<User, UserResponse>();
        }
    }
}