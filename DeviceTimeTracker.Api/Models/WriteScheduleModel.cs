﻿namespace DeviceTimeTracker.Api.Models
{
    public class WriteScheduleModel
    {
        public string UserId { get; set; }
        public ScheduleIntervalRawModel[] Intervals { get; set; }
    }
}
