﻿namespace DeviceTimeTracker.Api.Models
{
    public class WriteUserModel
    {
        public string UserName { get; set; }
        public string Workstation { get; set; }
        public bool Active { get; set; }
    }
}
