﻿using AutoMapper;
using DeviceTimeTracker.Api.Models;
using DeviceTimeTracker.Domain.Models;
using DeviceTimeTracker.Domain.Requests.ActivityLog;
using DeviceTimeTracker.Domain.Responses.ActivityLog;

namespace DeviceTimeTracker.Api.Mapper
{
    public class ActivityLogProfile : Profile
    {
        public ActivityLogProfile()
        {
            CreateMap<WriteActivityLogModel, PostActivityLogRequest>();
            CreateMap<PostActivityLogResponse, ReadActivityLogModel>();

            CreateMap<GetActivityLogDetailedResponse, ReadActivityLogModelDetails>();
            CreateMap<DetailedActivityLog, GetActivityLogDetailedResponse>();
        }
    }
}
