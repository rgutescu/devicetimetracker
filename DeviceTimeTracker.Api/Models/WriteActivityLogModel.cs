﻿using System;

namespace DeviceTimeTracker.Api.Models
{
    public class WriteActivityLogModel
    {
        public string UserId { get; set; }
        public DateTime Timestamp { get; set; }
        public bool IsInactive { get; set; }
    }
}
