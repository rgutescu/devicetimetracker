﻿using System;
using System.Net;
using System.Threading.Tasks;
using DeviceTimeTracker.Domain.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace DeviceTimeTracker.Api.Middleware
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ExceptionMiddleware> _logger;

        public ExceptionMiddleware(RequestDelegate next, ILogger<ExceptionMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }
        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (EntityNotFoundException nfEx)
            {
                await HandleExceptionAsync(httpContext, nfEx, HttpStatusCode.NotFound, nfEx.Message);
            }
            catch (InvalidModelException imEx)
            {
                await HandleExceptionAsync(httpContext, imEx, HttpStatusCode.BadRequest, imEx.Message);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(httpContext, ex);
            }
        }
        private Task HandleExceptionAsync(HttpContext context, Exception exception, 
            HttpStatusCode statusCode = HttpStatusCode.InternalServerError, string detail = null)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int) statusCode;

            var details = new ProblemDetails
            {
                Status = context.Response.StatusCode, 
                Detail = detail ?? exception.Message,
            };

            var jsonMessage = JsonConvert.SerializeObject(details,
                new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, CheckAdditionalContent = false });

            _logger.LogError(details.Detail, exception);

            return context.Response.WriteAsync(jsonMessage);
        }
    }
}
