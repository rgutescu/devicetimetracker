﻿using System.Threading.Tasks;
using DeviceTimeTracker.Domain.Requests.Schedule;
using DeviceTimeTracker.Domain.Responses.Schedule;

namespace DeviceTimeTracker.Domain.RequestHandlers.Abstract
{
    public interface IQueryScheduleRequestHandler
    {
        Task<ScheduleResponse> GetScheduleByUserId(GetScheduleRequest request);
        Task<UserScheduleStatusResponse> GetUserScheduleCurrentStatus(GetScheduleStatusRequest request);
    }
}
