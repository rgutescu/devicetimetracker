﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using DeviceTimeTracker.Domain.RequestHandlers.Abstract;
using DeviceTimeTracker.Domain.Requests.ActivityLog;
using DeviceTimeTracker.Domain.Responses.ActivityLog;
using DeviceTimeTracker.Domain.Services.Abstract;

namespace DeviceTimeTracker.Domain.RequestHandlers
{
    public class CommandActivityLogRequestHandler : ICommandActivityLogRequestHandler
    {
        private readonly IActivityLogService activityLogService;
        private readonly IMapper mapper;

        public CommandActivityLogRequestHandler(
            IActivityLogService activityLogService,
            IMapper mapper)
        {
            this.activityLogService = activityLogService;
            this.mapper = mapper;
        }

        public async Task<PostActivityLogResponse> PostActivity(PostActivityLogRequest request)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var user = mapper.Map<Models.ActivityLog>(request);

            var result = await activityLogService.CreateAsync(user);

            return mapper.Map<PostActivityLogResponse>(result);
        }
    }
}
