﻿using DeviceTimeTracker.Domain.Models;
using DeviceTimeTracker.Infrastructure.Context.Abstract;
using DeviceTimeTracker.Infrastructure.Database.Abstract;
using MongoDB.Driver;

namespace DeviceTimeTracker.Infrastructure.Context
{
    public class ScheduleContext : BaseContext<Schedule>, IScheduleContext
    {
        private const string CollectionName = "Schedule";

        public ScheduleContext(IMongoDbOptions options) 
            : base(options?.ConnectionString, options?.DatabaseName, CollectionName)
        {
        }

        public IMongoCollection<Schedule> ScheduleCollection => Entities;
    }
}