﻿using System.Threading.Tasks;
using DeviceTimeTracker.Domain.Models;
using DeviceTimeTracker.Domain.Repositories;
using DeviceTimeTracker.Infrastructure.Context.Abstract;
using MongoDB.Driver;

namespace DeviceTimeTracker.Infrastructure.Repositories
{
    public class ScheduleRepository : IScheduleRepository
    {
        private readonly IScheduleContext context;

        public ScheduleRepository(IScheduleContext context)
        {
            this.context = context;
        }

        public async Task<Schedule> GetByUserIdAsync(string userId)
        {
            return await context.ScheduleCollection.Find(s => s.UserId == userId).SingleOrDefaultAsync();
        }

        public async Task<Schedule> CreateAsync(Schedule schedule)
        {
            if (schedule == null)
            {
                return null;
            }

            await context.ScheduleCollection.InsertOneAsync(schedule);

            return schedule;
        }

        public async Task<Schedule> UpdateAsync(Schedule schedule)
        {
            if (schedule == null)
            {
                return null;
            }

            await context.ScheduleCollection.ReplaceOneAsync(u => u.Id == schedule.Id, schedule);
            return schedule;
        }
    }
}