﻿using System;
using System.Threading.Tasks;
using DeviceTimeTracker.Domain.Models;

namespace DeviceTimeTracker.Domain.Services.Abstract
{
    public interface IScheduleOverrideService
    {
        Task<ScheduleOverride> GetByUserIdAsync(string userId, DateTime date);
        Task<ScheduleOverride> SetAsync(ScheduleOverride scheduleOverride);
    }
}