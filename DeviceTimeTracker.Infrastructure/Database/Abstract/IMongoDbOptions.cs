﻿namespace DeviceTimeTracker.Infrastructure.Database.Abstract
{
    public interface IMongoDbOptions
    {
        string ConnectionString { get; }
        string DatabaseName { get; }
    }
}