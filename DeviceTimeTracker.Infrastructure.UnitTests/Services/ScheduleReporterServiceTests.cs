﻿using System.Threading.Tasks;
using DeviceTimeTracker.Domain.Exceptions;
using DeviceTimeTracker.Domain.Models;
using DeviceTimeTracker.Domain.Services.Abstract;
using DeviceTimeTracker.Infrastructure.Services;
using Moq;
using NUnit.Framework;

namespace DeviceTimeTracker.Infrastructure.UnitTests.Services
{
    [TestFixture]
    public class ScheduleReporterServiceTests
    {
        private ScheduleReporterService target;

        private Mock<IScheduleService> scheduleMock;
        private Mock<IScheduleOverrideService> scheduleOverrideServiceMock;
        private Mock<IUserService> userServiceMock;
        private Mock<IActivityLogService> activityLogServiceMock;

        [SetUp]
        public void SetUp()
        {
            scheduleMock = new Mock<IScheduleService>();
            scheduleOverrideServiceMock = new Mock<IScheduleOverrideService>();
            userServiceMock = new Mock<IUserService>();
            activityLogServiceMock = new Mock<IActivityLogService>();

            target = new ScheduleReporterService(scheduleMock.Object, scheduleOverrideServiceMock.Object, userServiceMock.Object, activityLogServiceMock.Object);
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase("9999")]
        public void GetUserScheduleCurrentStatus_When_InvalidUser_ThrowsException(string userId)
        {
            userServiceMock
                .Setup(x => x.GetByIdAsync(userId))
                .ReturnsAsync((User)null);

            Assert.ThrowsAsync<EntityNotFoundException>(() => target.GetUserScheduleCurrentStatus(userId));
        }

        [Test]
        public async Task GetUserScheduleCurrentStatus_When_InactiveUser_Returns_Disabled()
        {
            var userId = "1";
            var user = BuildUserDetails(userId, false);

            userServiceMock
                .Setup(x => x.GetByIdAsync(userId))
                .ReturnsAsync((user));

            var result = await target.GetUserScheduleCurrentStatus(userId);

            Assert.AreEqual(false, result.IsActive, "User should not be active");
            Assert.AreEqual(userId, result.UserId, "UserId does not match");
        }

        private User BuildUserDetails(string userId, bool isActive)
        {
            return new User
            {
                Id = userId,
                Active = isActive
            };
        }
    }
}
