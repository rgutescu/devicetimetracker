﻿namespace DeviceTimeTracker.Domain.Responses.Schedule
{
    public class UserScheduleStatusResponse
    {
        public string UserId { get; set; }
        public bool IsActive { get; set; }
        public bool IsAllowedBySchedule { get; set; }
    }
}
