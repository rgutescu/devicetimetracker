﻿using DeviceTimeTracker.Domain.Requests.Schedule;

namespace DeviceTimeTracker.Domain.Responses.Schedule
{
    public class ScheduleResponse
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public ScheduleIntervalDto[] Intervals { get; set; }
    }
}
