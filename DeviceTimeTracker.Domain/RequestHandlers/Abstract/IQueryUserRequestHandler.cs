﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DeviceTimeTracker.Domain.Requests.User;
using DeviceTimeTracker.Domain.Responses.User;

namespace DeviceTimeTracker.Domain.RequestHandlers.Abstract
{
    public interface IQueryUserRequestHandler
    {
        Task<IEnumerable<UserResponse>> GetUsers();

        Task<UserResponse> GetUserById(GetUserRequest request);
    }
}
