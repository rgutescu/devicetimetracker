﻿using System;

namespace DeviceTimeTracker.Api.Models
{
    public class WriteScheduleOverrideModel
    {
        public string UserId { get; set; }
        public DateTime Date { get; set; }
        public ScheduleOverrideIntervalRawModel Interval { get; set; }
    }
}
