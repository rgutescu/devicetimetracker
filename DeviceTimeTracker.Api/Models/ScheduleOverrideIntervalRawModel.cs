﻿using System.ComponentModel.DataAnnotations;
using DeviceTimeTracker.Domain;

namespace DeviceTimeTracker.Api.Models
{
    public class ScheduleOverrideIntervalRawModel
    {
        
        [RegularExpression(Constants.Regexp.HourRegexPattern, ErrorMessage = "Format: hh:mm")]
        public string StartTime { get; set; }

        [RegularExpression(Constants.Regexp.HourRegexPattern, ErrorMessage = "Format: hh:mm")]
        public string EndTime { get; set; }

        public int AllowedMinutes { get; set; }
    }
}
