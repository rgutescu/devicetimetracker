﻿using System.Threading.Tasks;
using DeviceTimeTracker.Domain.Models;

namespace DeviceTimeTracker.Domain.Repositories
{
    public interface IScheduleRepository
    {
        Task<Schedule> GetByUserIdAsync(string userId);
        Task<Schedule> CreateAsync(Schedule schedule);
        Task<Schedule> UpdateAsync(Schedule schedule);
    }
}