﻿using System;
using AutoMapper;
using DeviceTimeTracker.Api.Models;
using DeviceTimeTracker.Domain.Models;
using DeviceTimeTracker.Domain.Requests.Schedule;
using DeviceTimeTracker.Domain.Requests.ScheduleOverride;
using DeviceTimeTracker.Domain.Responses.Schedule;
using DeviceTimeTracker.Domain.Responses.ScheduleOverride;
using UserScheduleStatusModel = DeviceTimeTracker.Api.Models.UserScheduleStatusModel;

namespace DeviceTimeTracker.Api.Mapper
{
    public class ScheduleProfile : Profile
    {
        public ScheduleProfile()
        {
            CreateMap<WriteScheduleModel, PutScheduleRequest>();
            CreateMap<WriteScheduleOverrideModel, PutScheduleOverrideRequest>()
                .ForMember(dest => dest.Date, opt => 
                    opt.MapFrom(src => src.Date.ToLocalTime().Date));

            CreateMap<ScheduleIntervalRawModel, ScheduleIntervalDto>()
                .ForMember(dest => dest.Start,
                    opt => opt.MapFrom(src => FromRawToTimeSpan(src.StartTime)))
                .ForMember(dest => dest.End,
                    opt => opt.MapFrom(src => FromRawToTimeSpan(src.EndTime)));
            CreateMap<ScheduleOverrideIntervalRawModel, ScheduleOverrideIntervalDto>()
                .ForMember(dest => dest.Start,
                    opt => opt.MapFrom(src => FromRawToTimeSpan(src.StartTime)))
                .ForMember(dest => dest.End,
                    opt => opt.MapFrom(src => FromRawToTimeSpan(src.EndTime)));

            CreateMap<ScheduleIntervalDto, ScheduleIntervalRawModel>()
                .ForMember(dest => dest.StartTime,
                    opt => opt.MapFrom(src => FromTimespanToRaw(src.Start)))
                .ForMember(dest => dest.EndTime,
                    opt => opt.MapFrom(src => FromTimespanToRaw(src.End)));
            CreateMap<ScheduleOverrideIntervalDto, ScheduleOverrideIntervalRawModel>()
                .ForMember(dest => dest.StartTime,
                    opt => opt.MapFrom(src => FromTimespanToRaw(src.Start)))
                .ForMember(dest => dest.EndTime,
                    opt => opt.MapFrom(src => FromTimespanToRaw(src.End)));


            CreateMap<ScheduleIntervalDto, ScheduleIntervalModel>().ReverseMap();

            CreateMap<ScheduleResponse, ReadScheduleModel>();
            CreateMap<ScheduleOverrideResponse, ReadScheduleOverrideModel>();

            CreateMap<UserScheduleStatusResponse, UserScheduleStatusModel>();
        }

        private static TimeSpan FromRawToTimeSpan(string value)
        {
            var parsedTime = value.Split((':'));

            return new TimeSpan(int.Parse(parsedTime[0]), int.Parse(parsedTime[1]), 0);
        }

        private static string FromTimespanToRaw(TimeSpan value)
        {
            return $"{FormatDoubleDigit(value.Hours)}:{FormatDoubleDigit(value.Minutes)}";
        }

        private static string FormatDoubleDigit(int value)
        {
            return (value < 10) ? $"0{value}" : $"{value}";
        }
    }
}
