﻿using System;

namespace DeviceTimeTracker.Domain.Exceptions
{
    public class InvalidModelException : ApplicationException
    {
        public InvalidModelException(string message) : base(message)
        {

        }
    }
}
