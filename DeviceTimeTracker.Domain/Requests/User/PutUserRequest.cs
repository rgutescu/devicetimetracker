﻿namespace DeviceTimeTracker.Domain.Requests.User
{
    public class PutUserRequest
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Workstation { get; set; }
        public bool Active { get; set; }
    }
}
