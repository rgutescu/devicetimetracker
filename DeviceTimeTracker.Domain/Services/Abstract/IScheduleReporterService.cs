﻿using System.Threading.Tasks;
using DeviceTimeTracker.Domain.Models;

namespace DeviceTimeTracker.Domain.Services.Abstract
{
    public interface IScheduleReporterService
    {
        Task<UserScheduleStatusModel> GetUserScheduleCurrentStatus(string userId);
    }
}