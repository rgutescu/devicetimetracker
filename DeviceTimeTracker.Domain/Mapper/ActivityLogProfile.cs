﻿using AutoMapper;
using DeviceTimeTracker.Domain.Models;
using DeviceTimeTracker.Domain.Requests.ActivityLog;
using DeviceTimeTracker.Domain.Responses.ActivityLog;

namespace DeviceTimeTracker.Domain.Mapper
{
    public class ActivityLogProfile : Profile
    {
        public ActivityLogProfile()
        {
            CreateMap<PostActivityLogRequest, ActivityLog>();
            CreateMap<ActivityLog, PostActivityLogResponse>();

            CreateMap<DetailedActivityLog, GetActivityLogDetailedResponse>();
        }
    }
}