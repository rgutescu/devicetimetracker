﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using System.Threading.Tasks;
using DeviceTimeTracker.Api.Models;
using DeviceTimeTracker.Domain.RequestHandlers.Abstract;
using DeviceTimeTracker.Domain.Requests.Schedule;

namespace DeviceTimeTracker.Api.Controllers
{
    [Route("api/v{version:apiVersion}/user/")]
    [ApiVersion("1")]
    [ApiController]
    public class ScheduleController : ControllerBase
    {
        private readonly ICommandScheduleRequestHandler commandScheduleRequestHandler;
        private readonly IQueryScheduleRequestHandler queryScheduleRequestHandler;
        private readonly IMapper mapper;

        public ScheduleController(
            IQueryScheduleRequestHandler queryScheduleRequestHandler,
            ICommandScheduleRequestHandler commandScheduleRequestHandler,
            IMapper mapper)
        {
            this.commandScheduleRequestHandler = commandScheduleRequestHandler;
            this.queryScheduleRequestHandler = queryScheduleRequestHandler;
            this.mapper = mapper;
        }

        // GET: api/v1/user/123/schedule/
        [HttpGet("{userId}/[controller]")]
        [ProducesResponseType(typeof(ReadScheduleModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetScheduleByUserId([FromRoute] string userId)
        {
            var result =
                await queryScheduleRequestHandler.GetScheduleByUserId(new GetScheduleRequest {UserId = userId});

            if (result == default)
            {
                return NotFound();
            }

            return Ok(mapper.Map<ReadScheduleModel>(result));
        }

        // PUT: api/v1/user/123/schedule
        [HttpPut("{userId}/[controller]")]
        [ProducesResponseType(typeof(ReadScheduleModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> PutSchedule([FromBody] WriteScheduleModel user, [FromRoute] string userId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var putScheduleRequest = mapper.Map<PutScheduleRequest>(user);
            putScheduleRequest.UserId = userId;

            var result = await commandScheduleRequestHandler.PutSchedule(putScheduleRequest);

            return Ok(mapper.Map<ReadScheduleModel>(result));
        }

        // GET: api/v1/user/123/schedule/status
        [HttpGet("{userId}/[controller]/status")]
        [ProducesResponseType(typeof(UserScheduleStatusModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetUserScheduleCurrentStatus([FromRoute] string userId)
        {
            var result =
                await queryScheduleRequestHandler.GetUserScheduleCurrentStatus(new GetScheduleStatusRequest { UserId = userId });

            return Ok(mapper.Map<UserScheduleStatusModel>(result));
        }
    }
}
