﻿using System;

namespace DeviceTimeTracker.Domain.Requests.ScheduleOverride
{
    public class PutScheduleOverrideRequest
    {
        public string UserId { get; set; }
        public DateTime Date { get; set; }
        public ScheduleOverrideIntervalDto Interval { get; set; }
    }
}
