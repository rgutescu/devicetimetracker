﻿using System;

namespace DeviceTimeTracker.Domain.Requests.ScheduleOverride
{
    public class ScheduleOverrideIntervalDto
    {
        public TimeSpan Start;
        public TimeSpan End;
        public int AllowedMinutes;
    }
}
