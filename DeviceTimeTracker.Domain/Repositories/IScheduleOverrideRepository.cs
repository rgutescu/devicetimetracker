﻿using System;
using System.Threading.Tasks;
using DeviceTimeTracker.Domain.Models;

namespace DeviceTimeTracker.Domain.Repositories
{
    public interface IScheduleOverrideRepository
    {
        Task<ScheduleOverride> GetByUserIdAsync(string userId, DateTime date);
        Task<ScheduleOverride> CreateAsync(ScheduleOverride scheduleOverride);
        Task<ScheduleOverride> UpdateAsync(ScheduleOverride scheduleOverride);
    }
}