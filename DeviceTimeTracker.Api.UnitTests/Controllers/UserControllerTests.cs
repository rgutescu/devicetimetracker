using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using DeviceTimeTracker.Api.Controllers;
using DeviceTimeTracker.Api.Models;
using DeviceTimeTracker.Domain.RequestHandlers.Abstract;
using DeviceTimeTracker.Domain.Requests.User;
using DeviceTimeTracker.Domain.Responses.User;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace DeviceTimeTracker.Api.UnitTests.Controllers
{
    [TestFixture]
    public class UserControllerTests
    {
        private UserController target;

        private Mock<IQueryUserRequestHandler> queryUserRequestHandlerMock;
        private Mock<ICommandUserRequestHandler> commandUserRequestHandlerMock;
        private Mock<IMapper> mapperMock;

        [SetUp]
        public void SetUp()
        {
            queryUserRequestHandlerMock = new Mock<IQueryUserRequestHandler>();
            commandUserRequestHandlerMock = new Mock<ICommandUserRequestHandler>();

            mapperMock = new Mock<IMapper>();

            target = new UserController(queryUserRequestHandlerMock.Object, commandUserRequestHandlerMock.Object, mapperMock.Object);
        }

        [Test]
        public async Task GetUsers_Returns_Results()
        {
            queryUserRequestHandlerMock.Setup(x => x.GetUsers())
                .ReturnsAsync(new List<UserResponse>());

            var result = await target.GetUsers();

            Assert.IsNotNull(result as OkObjectResult);

            queryUserRequestHandlerMock.Verify(x => x.GetUsers(), Times.Once);
            queryUserRequestHandlerMock.VerifyNoOtherCalls();
            commandUserRequestHandlerMock.VerifyNoOtherCalls();
        }

        [Test]
        public async Task GetUserById_Returns_NotFound()
        {
            var userId = "1";
            queryUserRequestHandlerMock.Setup(x => x.GetUserById(It.IsAny<GetUserRequest>()))
                .ReturnsAsync(default(UserResponse));

            var result = await target.GetUserById(userId);

            Assert.IsNotNull(result as NotFoundResult);

            queryUserRequestHandlerMock.Verify(x => x.GetUserById(It.IsAny<GetUserRequest>()), Times.Once);
            commandUserRequestHandlerMock.VerifyNoOtherCalls();
            commandUserRequestHandlerMock.VerifyNoOtherCalls();
        }

        [Test]
        public async Task GetScheduleByUserId_Returns_Results()
        {
            var userId = "1";
            queryUserRequestHandlerMock.Setup(x => x.GetUserById(It.IsAny<GetUserRequest>()))
                .ReturnsAsync(new UserResponse());

            var result = await target.GetUserById(userId);

            Assert.IsNotNull(result as OkObjectResult);

            queryUserRequestHandlerMock.Verify(x => x.GetUserById(It.IsAny<GetUserRequest>()), Times.Once);
            commandUserRequestHandlerMock.VerifyNoOtherCalls();
            commandUserRequestHandlerMock.VerifyNoOtherCalls();
        }

        [Test]
        public async Task PostUser_Returns_Results()
        {
            commandUserRequestHandlerMock.Setup(x => x.PostUser(It.IsAny<PostUserRequest>()))
                .ReturnsAsync(new UserResponse());

            var result = await target.PostUser(new WriteUserModel());

            Assert.IsNotNull(result as OkObjectResult);

            commandUserRequestHandlerMock.Verify(x => x.PostUser(It.IsAny<PostUserRequest>()), Times.Once);
            queryUserRequestHandlerMock.VerifyNoOtherCalls();
            commandUserRequestHandlerMock.VerifyNoOtherCalls();
        }

        [Test]
        public async Task PutUser_Returns_Results()
        {
            var userId = "1";
            commandUserRequestHandlerMock.Setup(x => x.PutUser(It.IsAny<PutUserRequest>()))
                .ReturnsAsync(new UserResponse());
            mapperMock.Setup(x => x.Map<PutUserRequest>(It.IsAny<WriteUserModel>()))
                .Returns(new PutUserRequest());

            var result = await target.PutUser(userId, new WriteUserModel());

            Assert.IsNotNull(result as OkObjectResult);

            commandUserRequestHandlerMock.Verify(x => x.PutUser(It.IsAny<PutUserRequest>()), Times.Once);
            queryUserRequestHandlerMock.VerifyNoOtherCalls();
            commandUserRequestHandlerMock.VerifyNoOtherCalls();
        }
    }
}
