using System;
using System.Threading.Tasks;
using AutoMapper;
using DeviceTimeTracker.Domain.Exceptions;
using DeviceTimeTracker.Domain.RequestHandlers;
using DeviceTimeTracker.Domain.Requests.ActivityLog;
using DeviceTimeTracker.Domain.Services.Abstract;
using Moq;
using NUnit.Framework;

namespace DeviceTimeTracker.Domain.UnitTests.RequestHandlers
{
    [TestFixture]
    public class QueryActivityLogRequestHandlerTests
    {
        private QueryActivityLogRequestHandler target;

        private Mock<IActivityLogService> activityLogServiceMock;
        private Mock<IMapper> mapperMock;

        [SetUp]
        public void SetUp()
        {
            activityLogServiceMock = new Mock<IActivityLogService>();
            mapperMock = new Mock<IMapper>();

            target = new QueryActivityLogRequestHandler(activityLogServiceMock.Object, mapperMock.Object);
        }

        [Test]
        public void SearchActivities_WhenNull_Throws()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => target.SearchActivities(null));
        }

        [Test]
        public void SearchActivities_WhenInvalid_Throws()
        {
            var request = BuildInvalidRequest();
            Assert.ThrowsAsync<InvalidModelException>(() => target.SearchActivities(request));
        }

        [Test]
        public async Task SearchActivities_Calls_Service()
        {
            var request = BuildValidRequest();
            _ = await target.SearchActivities(request);

            activityLogServiceMock.Verify(x => x.SearchAsync(request.UserId, request.Start, request.End), Times.Once);
            activityLogServiceMock.VerifyNoOtherCalls();
        }

        private SearchActivityLogRequest BuildInvalidRequest()
        {
            return new SearchActivityLogRequest();
        }

        private SearchActivityLogRequest BuildValidRequest()
        {
            return new SearchActivityLogRequest
            {
                UserId = "112",
                Start = DateTime.Now.Date,
                End = DateTime.Now.Date.AddDays(1)
            };
        }
    }
}
