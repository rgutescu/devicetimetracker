﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DeviceTimeTracker.Domain.Models;

namespace DeviceTimeTracker.Domain.Repositories
{
    public interface IActivityLogRepository
    {
        Task<IEnumerable<ActivityLog>> SearchAsync(string userId, DateTime start, DateTime end);
        Task<ActivityLog> CreateAsync(ActivityLog activityLog);
    }
}