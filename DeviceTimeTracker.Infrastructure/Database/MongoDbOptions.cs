﻿using DeviceTimeTracker.Infrastructure.Database.Abstract;

namespace DeviceTimeTracker.Infrastructure.Database
{
    public class MongoDbOptions : IMongoDbOptions
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}