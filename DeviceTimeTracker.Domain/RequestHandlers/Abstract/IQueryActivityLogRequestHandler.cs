﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DeviceTimeTracker.Domain.Requests.ActivityLog;
using DeviceTimeTracker.Domain.Responses.ActivityLog;

namespace DeviceTimeTracker.Domain.RequestHandlers.Abstract
{
    public interface IQueryActivityLogRequestHandler
    {
        Task<IEnumerable<GetActivityLogDetailedResponse>> SearchActivities(SearchActivityLogRequest request);
    }
}
