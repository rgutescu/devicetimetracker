﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using System.Collections.Generic;
using System.Threading.Tasks;
using DeviceTimeTracker.Api.Models;
using DeviceTimeTracker.Domain.RequestHandlers.Abstract;
using DeviceTimeTracker.Domain.Requests.User;

namespace DeviceTimeTracker.Api.Controllers
{
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiVersion("1")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IQueryUserRequestHandler queryUserRequestHandler;
        private readonly ICommandUserRequestHandler commandUserRequestHandler;
        private readonly IMapper mapper;

        public UserController(
            IQueryUserRequestHandler queryUserRequestHandler,
            ICommandUserRequestHandler commandUserRequestHandler,
            IMapper mapper)
        {
            this.queryUserRequestHandler = queryUserRequestHandler;
            this.commandUserRequestHandler = commandUserRequestHandler;
            this.mapper = mapper;
        }

        // GET: api/v1/user
        [HttpGet("")]
        [ProducesResponseType(typeof(IEnumerable<ReadUserModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetUsers()
        {
            var result = await queryUserRequestHandler.GetUsers();

            return Ok(mapper.Map<IEnumerable<ReadUserModel>>(result));
        }

        // GET: api/v1/user/5
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ReadUserModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetUserById(string id)
        {
            var result = await queryUserRequestHandler.GetUserById(new GetUserRequest { Id = id });

            if (result == default)
            {
                return NotFound();
            }

            return Ok(mapper.Map<ReadUserModel>(result));
        }

        // POST: api/v1/user
        [HttpPost("")]
        [ProducesResponseType(typeof(ReadUserModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> PostUser([FromBody] WriteUserModel user)
        {
            var postUserRequest = mapper.Map<PostUserRequest>(user);
            var result = await commandUserRequestHandler.PostUser(postUserRequest);

            return Ok(mapper.Map<ReadUserModel>(result));
        }

        // PUT: api/v1/user/id
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(ReadUserModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> PutUser([FromRoute] string id, [FromBody] WriteUserModel user)
        {
            var putUserRequest = mapper.Map<PutUserRequest>(user);
            putUserRequest.Id = id;

            var result = await commandUserRequestHandler.PutUser(putUserRequest);

            return Ok(mapper.Map<ReadUserModel>(result));
        }
    }
}
