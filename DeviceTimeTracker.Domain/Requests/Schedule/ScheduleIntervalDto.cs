﻿using System;

namespace DeviceTimeTracker.Domain.Requests.Schedule
{
    public class ScheduleIntervalDto
    {
        public int Day { get; set; }
        public TimeSpan Start;
        public TimeSpan End;
        public int AllowedMinutes;
    }
}
