﻿using DeviceTimeTracker.Domain.Models;
using DeviceTimeTracker.Infrastructure.Context.Abstract;
using DeviceTimeTracker.Infrastructure.Database.Abstract;
using MongoDB.Driver;

namespace DeviceTimeTracker.Infrastructure.Context
{
    public class ScheduleOverrideContext : BaseContext<ScheduleOverride>, IScheduleOverrideContext
    {
        private const string CollectionName = "ScheduleOverride";

        public ScheduleOverrideContext(IMongoDbOptions options) 
            : base(options?.ConnectionString, options?.DatabaseName, CollectionName)
        {
        }

        public IMongoCollection<ScheduleOverride> ScheduleOverrideCollection => Entities;
    }
}