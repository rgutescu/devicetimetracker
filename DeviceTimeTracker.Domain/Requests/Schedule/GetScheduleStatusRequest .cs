﻿namespace DeviceTimeTracker.Domain.Requests.Schedule
{
    public class GetScheduleStatusRequest
    {
        public string UserId { get; set; }
    }
}
