﻿using System.Threading.Tasks;
using DeviceTimeTracker.Domain.Models;
using DeviceTimeTracker.Domain.Repositories;
using DeviceTimeTracker.Domain.Services.Abstract;

namespace DeviceTimeTracker.Infrastructure.Services
{
    public class ScheduleService : IScheduleService
    {
        private readonly IScheduleRepository scheduleRepository;

        public ScheduleService(IScheduleRepository scheduleRepository)
        {
            this.scheduleRepository = scheduleRepository;
        }

        public Task<Schedule> GetByUserIdAsync(string userId)
        {
            return scheduleRepository.GetByUserIdAsync(userId);
        }

        public async Task<Schedule> SetAsync(Schedule schedule)
        {
            var existingUserSchedule = await scheduleRepository.GetByUserIdAsync(schedule.UserId);

            if (existingUserSchedule == null)
            {
                return await scheduleRepository.CreateAsync(schedule);
            }

            schedule.Id = existingUserSchedule.Id;
            return await scheduleRepository.UpdateAsync(schedule);
        }
    }
}