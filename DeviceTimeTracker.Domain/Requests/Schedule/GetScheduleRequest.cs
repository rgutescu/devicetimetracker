﻿namespace DeviceTimeTracker.Domain.Requests.Schedule
{
    public class GetScheduleRequest
    {
        public string UserId { get; set; }
    }
}
