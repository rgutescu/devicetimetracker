﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using DeviceTimeTracker.Domain.Exceptions;
using DeviceTimeTracker.Domain.RequestHandlers.Abstract;
using DeviceTimeTracker.Domain.Requests.ActivityLog;
using DeviceTimeTracker.Domain.Responses.ActivityLog;
using DeviceTimeTracker.Domain.Services.Abstract;

namespace DeviceTimeTracker.Domain.RequestHandlers
{
    public class QueryActivityLogRequestHandler : IQueryActivityLogRequestHandler
    {
        private readonly IActivityLogService activityLogService;
        private readonly IMapper mapper;

        public QueryActivityLogRequestHandler(
            IActivityLogService activityLogService,
            IMapper mapper)
        {
            this.activityLogService = activityLogService;
            this.mapper = mapper;
        }

        public async Task<IEnumerable<GetActivityLogDetailedResponse>> SearchActivities(SearchActivityLogRequest request)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            if (!ValidateRequest(request, out string errorMessage))
            {
                throw new InvalidModelException(errorMessage);
            }

            var result = await activityLogService.SearchAsync(request.UserId, request.Start, request.End);

            var mappedResult = mapper.Map<IEnumerable<GetActivityLogDetailedResponse>>(result);

            return mappedResult;
        }

        private static bool ValidateRequest(SearchActivityLogRequest request, out string errorMessage)
        {
            errorMessage = null;

            if (string.IsNullOrWhiteSpace(request.UserId))
            {
                errorMessage = "UserId is mandatory";
                return false;
            }

            if (request.Start == default || request.End == default)
            {
                errorMessage = "Start and End dates are mandatory";
                return false;
            }

            if (request.End < request.Start)
            {
                errorMessage = "Invalid Start-End range";
                return false;
            }

            if (request.End - request.Start > TimeSpan.FromDays(Constants.Search.ActivitySearchMaxDaysInterval))
            {
                errorMessage = "Start - End dates interval should be a maximum of 30 days";
                return false;
            }

            return true;
        }
    }
}
