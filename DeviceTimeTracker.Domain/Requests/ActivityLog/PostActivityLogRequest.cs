﻿using System;

namespace DeviceTimeTracker.Domain.Requests.ActivityLog
{
    public class PostActivityLogRequest
    {
        public string UserId { get; set; }
        public DateTime Timestamp { get; set; }
        public bool IsInactive { get; set; }
    }
}
