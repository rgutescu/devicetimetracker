﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using DeviceTimeTracker.Domain.Exceptions;
using DeviceTimeTracker.Domain.Models;
using DeviceTimeTracker.Domain.RequestHandlers.Abstract;
using DeviceTimeTracker.Domain.Requests.ScheduleOverride;
using DeviceTimeTracker.Domain.Responses.ScheduleOverride;
using DeviceTimeTracker.Domain.Services.Abstract;

namespace DeviceTimeTracker.Domain.RequestHandlers
{
    public class CommandScheduleOverrideRequestHandler : ICommandScheduleOverrideRequestHandler
    {
        private readonly IScheduleOverrideService scheduleOverrideService;
        private readonly IMapper mapper;

        public CommandScheduleOverrideRequestHandler(
            IScheduleOverrideService scheduleOverrideService,
            IMapper mapper)
        {
            this.scheduleOverrideService = scheduleOverrideService;
            this.mapper = mapper;
        }

        public async Task<ScheduleOverrideResponse> PutScheduleOverride(PutScheduleOverrideRequest request)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            if (!IsValidSchedule(request, out var validationMessage))
            {
                throw new InvalidModelException(validationMessage);
            }

            var scheduleOverride = mapper.Map<ScheduleOverride>(request);

            var result = await scheduleOverrideService.SetAsync(scheduleOverride);

            return mapper.Map<ScheduleOverrideResponse>(result);
        }

        private bool IsValidSchedule(PutScheduleOverrideRequest request, out string validationMessage)
        {
            validationMessage = null;

            if (request.Interval.Start > request.Interval.End)
            {
                validationMessage = $"Date range interval is not valid. Start hour should be before End hour";

                return false;
            }

            return true;
        }
    }
}
