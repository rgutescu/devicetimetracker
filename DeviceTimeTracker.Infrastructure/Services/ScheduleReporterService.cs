﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DeviceTimeTracker.Domain.Exceptions;
using DeviceTimeTracker.Domain.Models;
using DeviceTimeTracker.Domain.Services.Abstract;

namespace DeviceTimeTracker.Infrastructure.Services
{
    public class ScheduleReporterService : IScheduleReporterService
    {
        private readonly IScheduleService scheduleService;
        private readonly IScheduleOverrideService scheduleOverrideService;
        private readonly IUserService userService;
        private readonly IActivityLogService activityLogService;

        public ScheduleReporterService(
            IScheduleService scheduleService,
            IScheduleOverrideService scheduleOverrideService,
            IUserService userService,
            IActivityLogService activityLogService)
        {
            this.scheduleService = scheduleService;
            this.scheduleOverrideService = scheduleOverrideService;
            this.userService = userService;
            this.activityLogService = activityLogService;
        }

        public async Task<UserScheduleStatusModel> GetUserScheduleCurrentStatus(string userId)
        {
            var userScheduleStatus = new UserScheduleStatusModel
            {
                UserId = userId,
                IsActive = true,
                IsAllowedBySchedule = true
            };

            var user = await userService.GetByIdAsync(userId);

            if (user == null)
            {
                throw new EntityNotFoundException($"User {userId} not found!");
            }

            if (!user.Active)
            {
                userScheduleStatus.IsActive = false;
                return userScheduleStatus;
            }

            var schedule = await scheduleService.GetByUserIdAsync(userId);
            if (schedule == null)
            {
                throw new EntityNotFoundException($"Schedule for {userId} not found!");
            }

            var timestamp = DateTime.Now;

            var totalSpentMinutes = await GetTotalSpentMinutes(userId, timestamp);

            var isAllowedByWeeklySchedule = IsAllowedByWeeklySchedule(totalSpentMinutes, schedule, timestamp);

            var scheduleOverride = await scheduleOverrideService.GetByUserIdAsync(userId, timestamp.Date);
            var isOverriddenSchedule = (scheduleOverride != null);
            var isAllowedByDailySchedule = IsAllowedByDailySchedule(totalSpentMinutes, scheduleOverride, timestamp);

            userScheduleStatus.IsAllowedBySchedule =
                (isOverriddenSchedule && isAllowedByDailySchedule) ||
                (!isOverriddenSchedule && isAllowedByWeeklySchedule);

            return userScheduleStatus;
        }

        private async Task<int> GetTotalSpentMinutes(string userId, DateTime timestamp)
        {
            var startDate = timestamp.Date;
            var endDate = startDate + TimeSpan.FromDays(1);
            
            var todayActivity = await activityLogService.SearchAsync(userId, startDate, endDate);

            return todayActivity.Sum(activity => activity.Duration);
        }

        private bool IsAllowedByWeeklySchedule(int totalSpentMinutes, Schedule schedule, DateTime timestamp)
        {
            var dayOfWeek = timestamp.DayOfWeek;
            var dayIndex = (dayOfWeek == DayOfWeek.Sunday) ? 7 : (int) dayOfWeek;

            var interval = schedule.Intervals.First(i => i.Day == dayIndex);

            return IsAllowedByInterval(totalSpentMinutes, timestamp, interval.Start, interval.End, interval.AllowedMinutes);
        }

        private bool IsAllowedByDailySchedule(int totalSpentMinutes, ScheduleOverride scheduleOverride, DateTime timestamp)
        {
            return scheduleOverride == null || 
                   IsAllowedByInterval(totalSpentMinutes, timestamp, scheduleOverride.Interval.Start, scheduleOverride.Interval.End, scheduleOverride.Interval.AllowedMinutes);
        }

        private bool IsAllowedByInterval(int totalSpentMinutes, DateTime timestamp, TimeSpan intervalStart, TimeSpan intervalEnd, int intervalAllowedMinutes)
        {
            var currentTime = new TimeSpan(timestamp.Hour, timestamp.Minute, 0);

            return (currentTime >= intervalStart) && 
                   (currentTime < intervalEnd) &&
                   (totalSpentMinutes < intervalAllowedMinutes);
        }
    }
}