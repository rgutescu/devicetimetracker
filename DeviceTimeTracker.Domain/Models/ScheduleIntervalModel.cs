﻿using System;

namespace DeviceTimeTracker.Domain.Models
{
    public class ScheduleIntervalModel
    {
        public int Day { get; set; }
        public TimeSpan Start;
        public TimeSpan End;
        public int AllowedMinutes;
    }
}
