﻿using DeviceTimeTracker.Domain.Models;
using MongoDB.Driver;

namespace DeviceTimeTracker.Infrastructure.Context.Abstract
{
    public interface IScheduleContext : IContext
    {
        IMongoCollection<Schedule> ScheduleCollection { get; }
    }
}