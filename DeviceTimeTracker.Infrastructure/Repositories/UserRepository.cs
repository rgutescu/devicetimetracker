﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DeviceTimeTracker.Domain.Models;
using DeviceTimeTracker.Domain.Repositories;
using DeviceTimeTracker.Infrastructure.Context.Abstract;
using MongoDB.Driver;

namespace DeviceTimeTracker.Infrastructure.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly IUserContext context;

        public UserRepository(IUserContext context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<User>> GetUsersAsync()
        {
            return await context.UserCollection.Find(FilterDefinition<User>.Empty).ToListAsync();
        }

        public async Task<User> GetByIdAsync(string id)
        {
            return await context.UserCollection.Find(u => u.Id == id).SingleOrDefaultAsync();
        }

        public async Task<User> CreateAsync(User user)
        {
            if (user == null)
            {
                return null;
            }

            await context.UserCollection.InsertOneAsync(user);

            return user;
        }

        public async Task<User> UpdateAsync(User user)
        {
            if (user == null)
            {
                return null;
            }

            await context.UserCollection.ReplaceOneAsync(u => u.Id == user.Id, user);
            return user;
        }
    }
}