﻿namespace DeviceTimeTracker.Domain.Responses.User
{
    public class UserResponse
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Workstation { get; set; }
        public bool Active { get; set; }
    }
}
