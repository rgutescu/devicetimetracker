# Device Time Tracker Service
> `Device Time Tracker Service` is created mainly to collect usage statistics and apply parental control policies (in terms of time usage) for registered users and various workstations

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Features](#features)
* [Status](#status)
* [Inspiration](#inspiration)
* [Contact](#contact)

## General info
Main purpose of the product is to track, analyze and provide parental control for any kind of users.
Functionality of this service is bases on REST API, so it can be used from any external device 

## Technologies
* .NET 5 (C#)
* MongoDB

## Setup
To run locally, requires instalation of .NET 5 SDK and of MongoDB server

## Features
* Users - register, edit details, enable/disable
* Schedule - define weekly usage schedule for a specific user
* Schedule override - define a daily override (custom specific usage schedule) for a specific user
* Activity - log device activity, retrieve usage statistics
* Parental control - determine if a user is allowed to use a device/workstation, based on scheduling rules and on daily usage

## To-do list:
* Create aggregated activity statistics (monthly, yearly)
* Implement security on API level
* Extend administrative capabilities

## Status
Project is: _in progress_, MVP functionality is done, but additional reporting capabilities are needed.

## Inspiration
Inspired by the need to have a more granular visibility of the time spent by children at the computer 

## Contact
Created by [@rgutescu]