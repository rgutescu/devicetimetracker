using System;
using DeviceTimeTracker.Domain.Repositories;
using DeviceTimeTracker.Infrastructure.Services;
using Moq;
using NUnit.Framework;
using System.Threading.Tasks;
using DeviceTimeTracker.Domain.Models;
using System.Collections.Generic;
using System.Linq;

namespace DeviceTimeTracker.Infrastructure.UnitTests.Services
{
    [TestFixture]
    public class ActivityLogServiceTests
    {
        private ActivityLogService target;

        private Mock<IActivityLogRepository> activityLogRepositoryMock;
        private Mock<IUserRepository> userRepositoryMock;

        [SetUp]
        public void SetUp()
        {
            activityLogRepositoryMock = new Mock<IActivityLogRepository>();
            userRepositoryMock = new Mock<IUserRepository>();

            target = new ActivityLogService(activityLogRepositoryMock.Object, userRepositoryMock.Object);
        }

        [Test]
        public async Task CreateAsync_Calls_Repository()
        {
            var activityLog = new ActivityLog { Id = "1", UserId = "2", Timestamp = DateTime.Now, IsInactive = false };

            _ = await target.CreateAsync(activityLog);

            activityLogRepositoryMock.Verify(x => x.CreateAsync(activityLog), Times.Once);
            activityLogRepositoryMock.VerifyNoOtherCalls();
        }

        [Test]
        public async Task SearchAsync_Calls_Repository()
        {
            var userId = "1";
            var start = DateTime.Now;
            var end = DateTime.Now.AddDays(1);

            _ = await target.SearchAsync(userId, start, end);

            activityLogRepositoryMock.Verify(x => x.SearchAsync(userId, start, end), Times.Once);
            activityLogRepositoryMock.VerifyNoOtherCalls();
        }

        [Test]
        public async Task SearchAsync_Returns_Correctly()
        {
            var userId = "1";
            var start = DateTime.Now.Date;
            var end = DateTime.Now.Date.AddDays(1);

            var startActivityDate = DateTime.Now.Date.AddHours(3);

            var activityLogResults = BuildActivityLogResults(userId, startActivityDate);
            var userDetails = BuildUserDetails(userId, true);

            activityLogRepositoryMock
                .Setup(x => x.SearchAsync(It.IsAny<string>(), It.IsAny<DateTime>(), It.IsAny<DateTime>()))
                .ReturnsAsync(activityLogResults);

            userRepositoryMock
                .Setup(x => x.GetByIdAsync(userId))
                .ReturnsAsync(userDetails);

            var result = await target.SearchAsync(userId, start, end);

            var expectedActivityLog = new DetailedActivityLog
            {
                UserId = userId,
                Workstation = "workstation",
                UserName = "username",
                StartTimestamp = startActivityDate,
                EndTimestamp = startActivityDate.AddMinutes(2),
                Duration = 2
            };

            var resultList = result.ToList();
            Assert.AreEqual(1, resultList.Count());
            var firstLog = resultList.First();
            Assert.AreEqual(expectedActivityLog.Duration, firstLog.Duration, "Logs Duration does not match");
            Assert.AreEqual(expectedActivityLog.StartTimestamp, firstLog.StartTimestamp, "Logs StartTimestamp does not match");
            Assert.AreEqual(expectedActivityLog.EndTimestamp, firstLog.EndTimestamp, "Logs EndTimestamp does not match");
            Assert.AreEqual(expectedActivityLog.UserId, firstLog.UserId, "Logs UserId does not match");
            Assert.AreEqual(expectedActivityLog.UserName, firstLog.UserName, "Logs UserName does not match");
            Assert.AreEqual(expectedActivityLog.Workstation, firstLog.Workstation, "Logs Workstation does not match");

            activityLogRepositoryMock.Verify(x => x.SearchAsync(userId, start, end), Times.Once);
            activityLogRepositoryMock.VerifyNoOtherCalls();
            userRepositoryMock.Verify(x => x.GetByIdAsync(userId), Times.Once);
            userRepositoryMock.VerifyNoOtherCalls();
        }

        private static List<ActivityLog> BuildActivityLogResults(string userId, DateTime startDate)
        {
            return new List<ActivityLog>
            {
                new ActivityLog {Id = "1", Timestamp = startDate, UserId = userId, IsInactive = false},
                new ActivityLog {Id = "2", Timestamp = startDate.AddMinutes(1), UserId = userId, IsInactive = false},
                new ActivityLog {Id = "3", Timestamp = startDate.AddMinutes(2), UserId = userId, IsInactive = false},
            };
        }

        private static User BuildUserDetails(string userId, bool active = true)
        {
            return new User
            {
                Id = userId,
                UserName = "username",
                Workstation = "workstation",
                Active = active
            };
        }
    }
}
