﻿namespace DeviceTimeTracker.Api.Models
{
    public class ReadUserModel : WriteUserModel
    {
        public string Id { get; set; }
    }
}
