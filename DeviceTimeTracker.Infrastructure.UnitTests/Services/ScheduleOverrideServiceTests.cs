using System;
using DeviceTimeTracker.Domain.Repositories;
using DeviceTimeTracker.Infrastructure.Services;
using Moq;
using NUnit.Framework;
using System.Threading.Tasks;
using DeviceTimeTracker.Domain.Models;

namespace DeviceTimeTracker.Infrastructure.UnitTests.Services
{
    [TestFixture]
    public class ScheduleOverrideServiceTests
    {
        private ScheduleOverrideService target;

        private Mock<IScheduleOverrideRepository> scheduleOverrideRepositoryMock;

        [SetUp]
        public void SetUp()
        {
            scheduleOverrideRepositoryMock = new Mock<IScheduleOverrideRepository>();

            target = new ScheduleOverrideService(scheduleOverrideRepositoryMock.Object);
        }

        [Test]
        public async Task GetByUserIdAsync_Calls_Repository()
        {
            var userId = "124";
            var date = DateTime.Now.Date;
            _ = await target.GetByUserIdAsync(userId, date);

            scheduleOverrideRepositoryMock.Verify(x => x.GetByUserIdAsync(userId, date), Times.Once);
            scheduleOverrideRepositoryMock.VerifyNoOtherCalls();
        }

        [Test]
        public async Task SetAsync_Calls_Repository_On_Create()
        {
            var userId = "124";
            var date = DateTime.Now.Date;
            var scheduleOverride = new ScheduleOverride {UserId = userId, Date = date};

            scheduleOverrideRepositoryMock.Setup(x => x.GetByUserIdAsync(userId, date))
                .ReturnsAsync((ScheduleOverride) null);

            _ = await target.SetAsync(scheduleOverride);

            scheduleOverrideRepositoryMock.Verify(x => x.GetByUserIdAsync(userId, date), Times.Once);
            scheduleOverrideRepositoryMock.Verify(x => x.CreateAsync(scheduleOverride), Times.Once);
            scheduleOverrideRepositoryMock.VerifyNoOtherCalls();
        }

        [Test]
        public async Task SetAsync_Calls_Repository_On_Update()
        {
            var id = "1";
            var userId = "124";
            var date = DateTime.Now.Date;
            var scheduleOverride = new ScheduleOverride { UserId = userId, Date = date, Id = id};

            scheduleOverrideRepositoryMock.Setup(x => x.GetByUserIdAsync(userId, date))
                .ReturnsAsync(scheduleOverride);

            _ = await target.SetAsync(scheduleOverride);

            scheduleOverrideRepositoryMock.Verify(x => x.GetByUserIdAsync(userId, date), Times.Once);
            scheduleOverrideRepositoryMock.Verify(x => 
                x.UpdateAsync(It.Is<ScheduleOverride>(s => s.Id == id)), Times.Once);
            scheduleOverrideRepositoryMock.VerifyNoOtherCalls();
        }
    }
}
