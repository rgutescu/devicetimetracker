﻿namespace DeviceTimeTracker.Api.Models
{
    public class ReadActivityLogModel : WriteActivityLogModel
    {
        public string Id { get; set; }
    }
}
