﻿using System;

namespace DeviceTimeTracker.Domain.Models
{
    public class ScheduleOverrideIntervalModel
    {
        public TimeSpan Start;
        public TimeSpan End;
        public int AllowedMinutes;
    }
}
