﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using System.Threading.Tasks;
using DeviceTimeTracker.Api.Models;
using DeviceTimeTracker.Domain.RequestHandlers.Abstract;
using DeviceTimeTracker.Domain.Requests.ScheduleOverride;

namespace DeviceTimeTracker.Api.Controllers
{
    [Route("api/v{version:apiVersion}/user")]
    [ApiVersion("1")]
    [ApiController]
    public class ScheduleOverrideController : ControllerBase
    {
        private readonly ICommandScheduleOverrideRequestHandler commandScheduleOverrideRequestHandler;
        private readonly IQueryScheduleOverrideRequestHandler queryScheduleOverrideRequestHandler;
        private readonly IMapper mapper;

        public ScheduleOverrideController(
            IQueryScheduleOverrideRequestHandler queryScheduleOverrideRequestHandler,
            ICommandScheduleOverrideRequestHandler commandScheduleOverrideRequestHandler,
            IMapper mapper)
        {
            this.commandScheduleOverrideRequestHandler = commandScheduleOverrideRequestHandler;
            this.queryScheduleOverrideRequestHandler = queryScheduleOverrideRequestHandler;
            this.mapper = mapper;
        }

        // GET: api/v1/user/123/scheduleoverride?date=2020-02-01
        [HttpGet("{userId}/[controller]")]
        [ProducesResponseType(typeof(ReadScheduleOverrideModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetScheduleOverrideByUserId([FromRoute] string userId, [FromQuery] DateTime date)
        {
            var result =
                await queryScheduleOverrideRequestHandler.GetScheduleOverrideByUserId(new GetScheduleOverrideRequest { UserId = userId, Date = date });

            if (result == default)
            {
                return NotFound();
            }

            return Ok(mapper.Map<ReadScheduleOverrideModel>(result));
        }

        // PUT: api/v1/user/1234/scheduleoverride
        [HttpPut("{userId}/[controller]")]
        [ProducesResponseType(typeof(ReadScheduleOverrideModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> PutScheduleOverride([FromBody] WriteScheduleOverrideModel user, [FromRoute] string userId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var putScheduleOverrideRequest = mapper.Map<PutScheduleOverrideRequest>(user);
            putScheduleOverrideRequest.UserId = userId;

            var result = await commandScheduleOverrideRequestHandler.PutScheduleOverride(putScheduleOverrideRequest);

            return Ok(mapper.Map<ReadScheduleOverrideModel>(result));
        }
    }
}
