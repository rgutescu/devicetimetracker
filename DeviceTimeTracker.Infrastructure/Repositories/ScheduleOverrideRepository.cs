﻿using System;
using System.Threading.Tasks;
using DeviceTimeTracker.Domain.Models;
using DeviceTimeTracker.Domain.Repositories;
using DeviceTimeTracker.Infrastructure.Context.Abstract;
using MongoDB.Driver;

namespace DeviceTimeTracker.Infrastructure.Repositories
{
    public class ScheduleOverrideRepository : IScheduleOverrideRepository
    {
        private readonly IScheduleOverrideContext context;

        public ScheduleOverrideRepository(IScheduleOverrideContext context)
        {
            this.context = context;
        }

        public async Task<ScheduleOverride> GetByUserIdAsync(string userId, DateTime date)
        {
            return await context.ScheduleOverrideCollection
                .Find(s => s.UserId == userId && s.Date == date).SingleOrDefaultAsync();
        }

        public async Task<ScheduleOverride> CreateAsync(ScheduleOverride scheduleOverride)
        {
            if (scheduleOverride == null)
            {
                return null;
            }

            await context.ScheduleOverrideCollection.InsertOneAsync(scheduleOverride);

            return scheduleOverride;
        }

        public async Task<ScheduleOverride> UpdateAsync(ScheduleOverride scheduleOverride)
        {
            if (scheduleOverride == null)
            {
                return null;
            }

            await context.ScheduleOverrideCollection.ReplaceOneAsync(u => u.Id == scheduleOverride.Id, scheduleOverride);
            return scheduleOverride;
        }
    }
}