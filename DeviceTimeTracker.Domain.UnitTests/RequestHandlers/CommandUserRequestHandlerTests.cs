using System;
using System.Threading.Tasks;
using AutoMapper;
using DeviceTimeTracker.Domain.Exceptions;
using DeviceTimeTracker.Domain.Models;
using DeviceTimeTracker.Domain.RequestHandlers;
using DeviceTimeTracker.Domain.Requests.User;
using DeviceTimeTracker.Domain.Services.Abstract;
using Moq;
using NUnit.Framework;

namespace DeviceTimeTracker.Domain.UnitTests.RequestHandlers
{
    [TestFixture]
    public class CommandUserRequestHandlerTests
    {
        private CommandUserRequestHandler target;

        private Mock<IUserService> userServiceMock;
        private Mock<IMapper> mapperMock;

        [SetUp]
        public void SetUp()
        {
            userServiceMock = new Mock<IUserService>();
            mapperMock = new Mock<IMapper>();

            target = new CommandUserRequestHandler(userServiceMock.Object, mapperMock.Object);
        }

        [Test]
        public void PostUser_WhenNull_Throws()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => target.PostUser(null));
        }

        [Test]
        public async Task PostUser_Calls_Service()
        {
            var request = new PostUserRequest();
            _ = await target.PostUser(request);

            userServiceMock.Verify(x => x.CreateAsync(It.IsAny<User>()), Times.Once);
            userServiceMock.VerifyNoOtherCalls();
        }

        [Test]
        public void PutUser_WhenNull_Throws()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => target.PutUser(null));
        }

        [Test]
        public void PutUser_WhenNotExisting_Throws()
        {
            var id = "1";
            var request = new PutUserRequest {Id = id};
            userServiceMock.Setup(x => x.GetByIdAsync(id)).ReturnsAsync((User) null);

            Assert.ThrowsAsync<EntityNotFoundException>(() => target.PutUser(request));
        }

        [Test]
        public async Task PutUser_Calls_Service()
        {
            var id = "1";
            var request = new PutUserRequest { Id = id };
            userServiceMock.Setup(x => x.GetByIdAsync(id)).ReturnsAsync(new User());

            _ = await target.PutUser(request);

            userServiceMock.Verify(x => x.UpdateAsync(It.IsAny<User>()), Times.Once);
            userServiceMock.Verify(x => x.GetByIdAsync(id), Times.Once);
            userServiceMock.VerifyNoOtherCalls();
        }
    }
}
