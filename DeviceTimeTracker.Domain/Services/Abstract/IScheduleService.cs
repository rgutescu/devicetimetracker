﻿using System.Threading.Tasks;
using DeviceTimeTracker.Domain.Models;

namespace DeviceTimeTracker.Domain.Services.Abstract
{
    public interface IScheduleService
    {
        Task<Schedule> GetByUserIdAsync(string userId);
        Task<Schedule> SetAsync(Schedule schedule);
    }
}