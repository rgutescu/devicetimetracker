using System;
using System.Threading.Tasks;
using AutoMapper;
using DeviceTimeTracker.Api.Controllers;
using DeviceTimeTracker.Api.Models;
using DeviceTimeTracker.Domain.RequestHandlers.Abstract;
using DeviceTimeTracker.Domain.Requests.ScheduleOverride;
using DeviceTimeTracker.Domain.Responses.ScheduleOverride;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace DeviceTimeTracker.Api.UnitTests.Controllers
{
    [TestFixture]
    public class ScheduleOverrideControllerTests
    {
        private ScheduleOverrideController target;

        private Mock<IQueryScheduleOverrideRequestHandler> queryScheduleOverrideRequestHandlerMock;
        private Mock<ICommandScheduleOverrideRequestHandler> commandScheduleOverrideRequestHandlerMock;
        private Mock<IMapper> mapperMock;

        [SetUp]
        public void SetUp()
        {
            queryScheduleOverrideRequestHandlerMock = new Mock<IQueryScheduleOverrideRequestHandler>();
            commandScheduleOverrideRequestHandlerMock = new Mock<ICommandScheduleOverrideRequestHandler>();

            mapperMock = new Mock<IMapper>();

            target = new ScheduleOverrideController(queryScheduleOverrideRequestHandlerMock.Object, commandScheduleOverrideRequestHandlerMock.Object, mapperMock.Object);
        }

        [Test]
        public async Task GetScheduleOverrideByUserId_Returns_NotFound()
        {
            var userId = "1";
            queryScheduleOverrideRequestHandlerMock.Setup(x => x.GetScheduleOverrideByUserId(It.IsAny<GetScheduleOverrideRequest>()))
                .ReturnsAsync(default(ScheduleOverrideResponse));
            
            var result = await target.GetScheduleOverrideByUserId(userId, DateTime.Now);

            Assert.IsNotNull(result as NotFoundResult);

            queryScheduleOverrideRequestHandlerMock.Verify(x => x.GetScheduleOverrideByUserId(It.IsAny<GetScheduleOverrideRequest>()), Times.Once);
            queryScheduleOverrideRequestHandlerMock.VerifyNoOtherCalls();
            commandScheduleOverrideRequestHandlerMock.VerifyNoOtherCalls();
        }

        [Test]
        public async Task GetScheduleOverrideByUserId_Returns_Results()
        {
            var userId = "1";
            queryScheduleOverrideRequestHandlerMock.Setup(x => x.GetScheduleOverrideByUserId(It.IsAny<GetScheduleOverrideRequest>()))
                .ReturnsAsync(new ScheduleOverrideResponse());

            var result = await target.GetScheduleOverrideByUserId(userId, DateTime.Now);

            Assert.IsNotNull(result as OkObjectResult);

            queryScheduleOverrideRequestHandlerMock.Verify(x => x.GetScheduleOverrideByUserId(It.IsAny<GetScheduleOverrideRequest>()), Times.Once);
            queryScheduleOverrideRequestHandlerMock.VerifyNoOtherCalls();
            commandScheduleOverrideRequestHandlerMock.VerifyNoOtherCalls();
        }

        [Test]
        public async Task PutScheduleOverride_Returns_Results()
        {
            var userId = "1";
            commandScheduleOverrideRequestHandlerMock.Setup(x => x.PutScheduleOverride(It.IsAny<PutScheduleOverrideRequest>()))
                .ReturnsAsync(new ScheduleOverrideResponse());
            mapperMock.Setup(x => x.Map<PutScheduleOverrideRequest>(It.IsAny<WriteScheduleOverrideModel>()))
                .Returns(new PutScheduleOverrideRequest());

            var result = await target.PutScheduleOverride(new WriteScheduleOverrideModel(), userId);

            Assert.IsNotNull(result as OkObjectResult);

            commandScheduleOverrideRequestHandlerMock.Verify(x => x.PutScheduleOverride(It.IsAny<PutScheduleOverrideRequest>()), Times.Once);
            queryScheduleOverrideRequestHandlerMock.VerifyNoOtherCalls();
            commandScheduleOverrideRequestHandlerMock.VerifyNoOtherCalls();
        }

    }
}
