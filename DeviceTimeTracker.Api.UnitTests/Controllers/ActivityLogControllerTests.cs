using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using DeviceTimeTracker.Api.Controllers;
using DeviceTimeTracker.Api.Models;
using DeviceTimeTracker.Domain.RequestHandlers.Abstract;
using DeviceTimeTracker.Domain.Requests.ActivityLog;
using DeviceTimeTracker.Domain.Responses.ActivityLog;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace DeviceTimeTracker.Api.UnitTests.Controllers
{
    [TestFixture]
    public class ActivityLogControllerTests
    {
        private ActivityLogController target;

        private Mock<IQueryActivityLogRequestHandler> queryActivityLogRequestHandlerMock;
        private Mock<ICommandActivityLogRequestHandler> commandActivityLogRequestHandlerMock;
        private Mock<IMapper> mapperMock;

        [SetUp]
        public void SetUp()
        {
            queryActivityLogRequestHandlerMock = new Mock<IQueryActivityLogRequestHandler>();
            commandActivityLogRequestHandlerMock = new Mock<ICommandActivityLogRequestHandler>();

            mapperMock = new Mock<IMapper>();

            target = new ActivityLogController(queryActivityLogRequestHandlerMock.Object, commandActivityLogRequestHandlerMock.Object, mapperMock.Object);
        }

        [Test]
        public async Task Search_Returns()
        {
            queryActivityLogRequestHandlerMock.Setup(x => x.SearchActivities(It.IsAny<SearchActivityLogRequest>()))
                .ReturnsAsync(new List<GetActivityLogDetailedResponse>());
            
            var result = await target.Search("1", DateTime.Now, DateTime.Now);

            Assert.IsNotNull(result as OkObjectResult);

            queryActivityLogRequestHandlerMock.Verify(x => x.SearchActivities(It.IsAny<SearchActivityLogRequest>()), Times.Once);
            queryActivityLogRequestHandlerMock.VerifyNoOtherCalls();
            commandActivityLogRequestHandlerMock.VerifyNoOtherCalls();
        }

        [Test]
        public async Task Post_Returns()
        {
            commandActivityLogRequestHandlerMock.Setup(x => x.PostActivity(It.IsAny<PostActivityLogRequest>()))
                .ReturnsAsync(new PostActivityLogResponse());

            var result = await target.Post(new WriteActivityLogModel());

            Assert.IsNotNull(result as OkObjectResult);

            commandActivityLogRequestHandlerMock.Verify(x => x.PostActivity(It.IsAny<PostActivityLogRequest>()), Times.Once);
            queryActivityLogRequestHandlerMock.VerifyNoOtherCalls();
            commandActivityLogRequestHandlerMock.VerifyNoOtherCalls();
        }
    }
}
