﻿namespace DeviceTimeTracker.Api.Models
{
    public class ReadScheduleOverrideModel : WriteScheduleOverrideModel
    {
        public string Id { get; set; }
    }
}
