using System;
using System.Threading.Tasks;
using AutoMapper;
using DeviceTimeTracker.Domain.RequestHandlers;
using DeviceTimeTracker.Domain.Requests.ScheduleOverride;
using DeviceTimeTracker.Domain.Services.Abstract;
using Moq;
using NUnit.Framework;

namespace DeviceTimeTracker.Domain.UnitTests.RequestHandlers
{
    [TestFixture]
    public class QueryScheduleOverrideRequestHandlerTests
    {
        private QueryScheduleOverrideRequestHandler target;

        private Mock<IScheduleOverrideService> scheduleOverrideServiceMock;
        private Mock<IMapper> mapperMock;

        [SetUp]
        public void SetUp()
        {
            scheduleOverrideServiceMock = new Mock<IScheduleOverrideService>();
            mapperMock = new Mock<IMapper>();

            target = new QueryScheduleOverrideRequestHandler(scheduleOverrideServiceMock.Object, mapperMock.Object);
        }

        [Test]
        public void GetScheduleOverrideByUserId_WhenNull_Throws()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => target.GetScheduleOverrideByUserId(null));
        }

        [Test]
        public async Task GetScheduleOverrideByUserId_Calls_Service()
        {
            var request = BuildValidRequest();
            _ = await target.GetScheduleOverrideByUserId(request);

            scheduleOverrideServiceMock.Verify(x => x.GetByUserIdAsync(request.UserId, request.Date), Times.Once);
            scheduleOverrideServiceMock.VerifyNoOtherCalls();
        }

        private GetScheduleOverrideRequest BuildValidRequest()
        {
            return new GetScheduleOverrideRequest
            {
               UserId = "222",
               Date = DateTime.Now
            };
        }
    }
}
