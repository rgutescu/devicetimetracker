﻿using System.Threading.Tasks;
using DeviceTimeTracker.Domain.Requests.Schedule;
using DeviceTimeTracker.Domain.Responses.Schedule;

namespace DeviceTimeTracker.Domain.RequestHandlers.Abstract
{
    public interface ICommandScheduleRequestHandler
    {
        Task<ScheduleResponse> PutSchedule(PutScheduleRequest request);
    }
}
