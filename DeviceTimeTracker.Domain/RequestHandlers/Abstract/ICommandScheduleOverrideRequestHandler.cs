﻿using System.Threading.Tasks;
using DeviceTimeTracker.Domain.Requests.ScheduleOverride;
using DeviceTimeTracker.Domain.Responses.ScheduleOverride;

namespace DeviceTimeTracker.Domain.RequestHandlers.Abstract
{
    public interface ICommandScheduleOverrideRequestHandler
    {
        Task<ScheduleOverrideResponse> PutScheduleOverride(PutScheduleOverrideRequest request);
    }
}
