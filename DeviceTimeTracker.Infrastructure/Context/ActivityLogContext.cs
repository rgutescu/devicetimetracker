﻿using DeviceTimeTracker.Domain.Models;
using DeviceTimeTracker.Infrastructure.Context.Abstract;
using DeviceTimeTracker.Infrastructure.Database.Abstract;
using MongoDB.Driver;

namespace DeviceTimeTracker.Infrastructure.Context
{
    public class ActivityLogContext : BaseContext<ActivityLog>, IActivityLogContext
    {
        private const string CollectionName = "ActivityLog";

        public ActivityLogContext(IMongoDbOptions options) 
            : base(options?.ConnectionString, options?.DatabaseName, CollectionName)
        {
        }

        public IMongoCollection<ActivityLog> ActivityLogCollection => Entities;
    }
}