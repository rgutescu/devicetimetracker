﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DeviceTimeTracker.Domain.Models;
using DeviceTimeTracker.Domain.Repositories;
using DeviceTimeTracker.Domain.Services.Abstract;

namespace DeviceTimeTracker.Infrastructure.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository userRepository;
        private readonly IScheduleRepository scheduleRepository;

        public UserService(IUserRepository userRepository, IScheduleRepository scheduleRepository)
        {
            this.userRepository = userRepository;
            this.scheduleRepository = scheduleRepository;
        }

        public Task<IEnumerable<User>> GetAsync()
        {
            return userRepository.GetUsersAsync();
        }

        public Task<User> GetByIdAsync(string id)
        {
            return userRepository.GetByIdAsync(id);
        }

        public async Task<User> CreateAsync(User user)
        {
            var createdUser = await userRepository.CreateAsync(user);
            var defaultSchedule = Schedule.Default(createdUser.Id);

            await scheduleRepository.CreateAsync(defaultSchedule);

            return createdUser;
        }

        public Task<User> UpdateAsync(User user)
        {
            return userRepository.UpdateAsync(user);
        }
    }
}