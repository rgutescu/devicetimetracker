﻿using System;

namespace DeviceTimeTracker.Domain.Responses.ActivityLog
{
    public class PostActivityLogResponse
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
