using System;
using System.Threading.Tasks;
using AutoMapper;
using DeviceTimeTracker.Domain.Exceptions;
using DeviceTimeTracker.Domain.Models;
using DeviceTimeTracker.Domain.RequestHandlers;
using DeviceTimeTracker.Domain.Requests.Schedule;
using DeviceTimeTracker.Domain.Services.Abstract;
using Moq;
using NUnit.Framework;

namespace DeviceTimeTracker.Domain.UnitTests.RequestHandlers
{
    [TestFixture]
    public class CommandScheduleRequestHandlerTests
    {
        private CommandScheduleRequestHandler target;

        private Mock<IScheduleService> scheduleServiceMock;
        private Mock<IMapper> mapperMock;

        [SetUp]
        public void SetUp()
        {
            scheduleServiceMock = new Mock<IScheduleService>();
            mapperMock = new Mock<IMapper>();

            target = new CommandScheduleRequestHandler(scheduleServiceMock.Object, mapperMock.Object);
        }

        [Test]
        public void PutSchedule_WhenNull_Throws()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => target.PutSchedule(null));
        }

        [Test]
        public void PutSchedule_WhenInvalid_Throws()
        {
            var request = BuildInvalidRequest();

            Assert.ThrowsAsync<InvalidModelException>(() => target.PutSchedule(request));
        }

        [Test]
        public async Task PutScheduleOverride_Calls_Service()
        {
            var request = BuildValidRequest();
            _ = await target.PutSchedule(request);

            scheduleServiceMock.Verify(x => x.SetAsync(It.IsAny<Schedule>()), Times.Once);
            scheduleServiceMock.VerifyNoOtherCalls();
        }

        private PutScheduleRequest BuildInvalidRequest()
        {
            return new PutScheduleRequest();
        }

        private PutScheduleRequest BuildValidRequest()
        {
            var request = new PutScheduleRequest
            {
                Intervals = new ScheduleIntervalDto[7]
            };
            for (var i = 1; i <= 7; i++)
            {
                request.Intervals[i - 1] = BuildValidInterval(i);
            }

            return request;
        }

        private ScheduleIntervalDto BuildValidInterval(int index)
            => new()
            {
                Day = index, Start = TimeSpan.FromHours(1), End = TimeSpan.FromHours(5), AllowedMinutes = 10
            };
    }
}
