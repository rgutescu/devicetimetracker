using System.Threading.Tasks;
using AutoMapper;
using DeviceTimeTracker.Api.Controllers;
using DeviceTimeTracker.Api.Models;
using DeviceTimeTracker.Domain.RequestHandlers.Abstract;
using DeviceTimeTracker.Domain.Requests.Schedule;
using DeviceTimeTracker.Domain.Responses.Schedule;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace DeviceTimeTracker.Api.UnitTests.Controllers
{
    [TestFixture]
    public class ScheduleControllerTests
    {
        private ScheduleController target;

        private Mock<IQueryScheduleRequestHandler> queryScheduleRequestHandlerMock;
        private Mock<ICommandScheduleRequestHandler> commandScheduleRequestHandlerMock;
        private Mock<IMapper> mapperMock;

        [SetUp]
        public void SetUp()
        {
            queryScheduleRequestHandlerMock = new Mock<IQueryScheduleRequestHandler>();
            commandScheduleRequestHandlerMock = new Mock<ICommandScheduleRequestHandler>();

            mapperMock = new Mock<IMapper>();

            target = new ScheduleController(queryScheduleRequestHandlerMock.Object, commandScheduleRequestHandlerMock.Object, mapperMock.Object);
        }

        [Test]
        public async Task GetScheduleByUserId_Returns_NotFound()
        {
            var userId = "1";
            queryScheduleRequestHandlerMock.Setup(x => x.GetScheduleByUserId(It.IsAny<GetScheduleRequest>()))
                .ReturnsAsync(default(ScheduleResponse));
            
            var result = await target.GetScheduleByUserId(userId);

            Assert.IsNotNull(result as NotFoundResult);

            queryScheduleRequestHandlerMock.Verify(x => x.GetScheduleByUserId(It.IsAny<GetScheduleRequest>()), Times.Once);
            queryScheduleRequestHandlerMock.VerifyNoOtherCalls();
            commandScheduleRequestHandlerMock.VerifyNoOtherCalls();
        }

        [Test]
        public async Task GetScheduleByUserId_Returns_Results()
        {
            var userId = "1";
            queryScheduleRequestHandlerMock.Setup(x => x.GetScheduleByUserId(It.IsAny<GetScheduleRequest>()))
                .ReturnsAsync(new ScheduleResponse());

            var result = await target.GetScheduleByUserId(userId);

            Assert.IsNotNull(result as OkObjectResult);

            queryScheduleRequestHandlerMock.Verify(x => x.GetScheduleByUserId(It.IsAny<GetScheduleRequest>()), Times.Once);
            queryScheduleRequestHandlerMock.VerifyNoOtherCalls();
            commandScheduleRequestHandlerMock.VerifyNoOtherCalls();
        }

        [Test]
        public async Task GetUserScheduleCurrentStatus_Returns_Results()
        {
            var userId = "1";
            queryScheduleRequestHandlerMock.Setup(x => x.GetUserScheduleCurrentStatus(It.IsAny<GetScheduleStatusRequest>()))
                .ReturnsAsync(new UserScheduleStatusResponse());

            var result = await target.GetUserScheduleCurrentStatus(userId);

            Assert.IsNotNull(result as OkObjectResult);

            queryScheduleRequestHandlerMock.Verify(x => x.GetUserScheduleCurrentStatus(It.IsAny<GetScheduleStatusRequest>()), Times.Once);
            queryScheduleRequestHandlerMock.VerifyNoOtherCalls();
            commandScheduleRequestHandlerMock.VerifyNoOtherCalls();
        }

        [Test]
        public async Task PutSchedule_Returns_Results()
        {
            var userId = "1";
            commandScheduleRequestHandlerMock.Setup(x => x.PutSchedule(It.IsAny<PutScheduleRequest>()))
                .ReturnsAsync(new ScheduleResponse());
            mapperMock.Setup(x => x.Map<PutScheduleRequest>(It.IsAny<WriteScheduleModel>()))
                .Returns(new PutScheduleRequest());

            var result = await target.PutSchedule(new WriteScheduleModel(), userId);

            Assert.IsNotNull(result as OkObjectResult);

            commandScheduleRequestHandlerMock.Verify(x => x.PutSchedule(It.IsAny<PutScheduleRequest>()), Times.Once);
            queryScheduleRequestHandlerMock.VerifyNoOtherCalls();
            commandScheduleRequestHandlerMock.VerifyNoOtherCalls();
        }

    }
}
