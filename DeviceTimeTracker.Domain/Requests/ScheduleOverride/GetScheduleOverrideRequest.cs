﻿using System;

namespace DeviceTimeTracker.Domain.Requests.ScheduleOverride
{
    public class GetScheduleOverrideRequest
    {
        public string UserId { get; set; }
        public DateTime Date { get; set; }
    }
}
