﻿using System.Threading.Tasks;
using DeviceTimeTracker.Domain.Requests.User;
using DeviceTimeTracker.Domain.Responses.User;

namespace DeviceTimeTracker.Domain.RequestHandlers.Abstract
{
    public interface ICommandUserRequestHandler
    {
        Task<UserResponse> PostUser(PostUserRequest request);
        Task<UserResponse> PutUser(PutUserRequest request);
    }
}
