﻿using AutoMapper;
using DeviceTimeTracker.Domain.Models;
using DeviceTimeTracker.Domain.Requests.Schedule;
using DeviceTimeTracker.Domain.Requests.ScheduleOverride;
using DeviceTimeTracker.Domain.Responses.Schedule;
using DeviceTimeTracker.Domain.Responses.ScheduleOverride;

namespace DeviceTimeTracker.Domain.Mapper
{
    public class ScheduleProfile : Profile
    {
        public ScheduleProfile()
        {
            CreateMap<PutScheduleRequest, Schedule>();
            CreateMap<Schedule, ScheduleResponse>();

            CreateMap<ScheduleIntervalModel, ScheduleIntervalDto>().ReverseMap();

            CreateMap<PutScheduleOverrideRequest, ScheduleOverride>();
            CreateMap<ScheduleOverride, ScheduleOverrideResponse>();

            CreateMap<ScheduleOverrideIntervalModel, ScheduleOverrideIntervalDto>().ReverseMap();

            CreateMap<UserScheduleStatusModel, UserScheduleStatusResponse>();
        }
    }
}