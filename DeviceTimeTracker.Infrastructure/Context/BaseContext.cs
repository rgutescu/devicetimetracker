﻿using MongoDB.Driver;

namespace DeviceTimeTracker.Infrastructure.Context
{
    public abstract class BaseContext<TDocument>
    {
        public IMongoDatabase Database { get; }
        public IMongoCollection<TDocument> Entities { get; }
        protected BaseContext(string connectionString, string databaseName, string collectionName)
        {
            var client = new MongoClient(connectionString);

            Database = client.GetDatabase(databaseName);
            Entities = Database.GetCollection<TDocument>(collectionName);
        }
    }
}