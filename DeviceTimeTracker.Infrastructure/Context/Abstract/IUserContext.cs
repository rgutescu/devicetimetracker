﻿using DeviceTimeTracker.Domain.Models;
using MongoDB.Driver;

namespace DeviceTimeTracker.Infrastructure.Context.Abstract
{
    public interface IUserContext : IContext
    {
        IMongoCollection<User> UserCollection { get; }
    }
}