﻿using DeviceTimeTracker.Domain.Models;
using DeviceTimeTracker.Infrastructure.Context.Abstract;
using DeviceTimeTracker.Infrastructure.Database.Abstract;
using MongoDB.Driver;

namespace DeviceTimeTracker.Infrastructure.Context
{
    public class UserContext : BaseContext<User>, IUserContext
    {
        private const string CollectionName = "User";

        public UserContext(IMongoDbOptions options) 
            : base(options?.ConnectionString, options?.DatabaseName, CollectionName)
        {
        }

        public IMongoCollection<User> UserCollection => Entities;
    }
}