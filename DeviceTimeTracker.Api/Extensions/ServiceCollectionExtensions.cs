﻿using DeviceTimeTracker.Infrastructure.Database;
using DeviceTimeTracker.Infrastructure.Database.Abstract;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Serilog;

namespace DeviceTimeTracker.Api.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddApplicationDependencies(this IServiceCollection services, IConfiguration configuration)
        {
            return services
                .AddApiConfiguration(configuration)
                .ConfigureAllOptions(configuration);
        }

        private static IServiceCollection ConfigureAllOptions(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddOptions();

            var mongoDbOptions = configuration.GetSection("MongoDb").Get<MongoDbOptions>();
            services.AddSingleton<IMongoDbOptions>(mongoDbOptions);

            return services;
        }

        private static IServiceCollection AddApiConfiguration(this IServiceCollection services, IConfiguration configuration)
        {
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .CreateLogger();

            services
                .AddCors(options =>
                    options.AddDefaultPolicy(
                        builder =>
                        {
                            builder
                                .AllowAnyOrigin()
                                .AllowAnyHeader()
                                .AllowAnyMethod();
                        }))
                .AddControllers()
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.NullValueHandling = NullValueHandling.Include;
                    options.SerializerSettings.Converters.Add(new StringEnumConverter());
                });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "DeviceTimeTracker.Api", Version = "v1" });
            });

            services.AddApiVersioning(o =>
            {
                o.AssumeDefaultVersionWhenUnspecified = true;
                o.ApiVersionReader = new UrlSegmentApiVersionReader();
            });

            services.AddAutoMapper(typeof(Startup).Assembly, typeof(Domain.Mapper.UserProfile).Assembly);

            services
                .AddRouting(options => options.LowercaseUrls = true)
                .AddResponseCaching()
                .AddHttpContextAccessor()
                .AddHealthChecks();

            return services;
        }
    }
}