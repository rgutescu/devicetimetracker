﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using DeviceTimeTracker.Domain.RequestHandlers.Abstract;
using DeviceTimeTracker.Domain.Requests.User;
using DeviceTimeTracker.Domain.Responses.User;
using DeviceTimeTracker.Domain.Services.Abstract;

namespace DeviceTimeTracker.Domain.RequestHandlers
{
    public class QueryUserRequestHandler : IQueryUserRequestHandler
    {
        private readonly IUserService userService;
        private readonly IMapper mapper;

        public QueryUserRequestHandler(
            IUserService userService,
            IMapper mapper)
        {
            this.userService = userService;
            this.mapper = mapper;
        }

        public async Task<IEnumerable<UserResponse>> GetUsers()
        {
            var result = await userService.GetAsync();

            return mapper.Map<IEnumerable<UserResponse>>(result);
        }

        public async Task<UserResponse> GetUserById(GetUserRequest request)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var result = await userService.GetByIdAsync(request.Id);

            return mapper.Map<UserResponse>(result);
        }
    }
}
