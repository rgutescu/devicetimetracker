﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DeviceTimeTracker.Domain.Models;
using DeviceTimeTracker.Domain.Repositories;
using DeviceTimeTracker.Infrastructure.Context.Abstract;
using MongoDB.Driver;

namespace DeviceTimeTracker.Infrastructure.Repositories
{
    public class ActivityLogRepository : IActivityLogRepository
    {
        private readonly IActivityLogContext context;

        public ActivityLogRepository(IActivityLogContext context)
        {
            this.context = context;
        }
        public async Task<IEnumerable<ActivityLog>> SearchAsync(string userId, DateTime start, DateTime end)
        {
            var activityLogCursor = await context.ActivityLogCollection
                .FindAsync(al => 
                    al.UserId == userId &&
                    al.Timestamp >= start &&
                    al.Timestamp <= end &&
                    al.IsInactive == false);

            return await activityLogCursor.ToListAsync();
        }

        public async Task<ActivityLog> CreateAsync(ActivityLog activityLog)
        {
            if (activityLog == null)
            {
                return null;
            }

            await context.ActivityLogCollection.InsertOneAsync(activityLog);

            return activityLog;
        }
    }
}