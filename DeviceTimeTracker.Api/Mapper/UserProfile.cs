﻿using AutoMapper;
using DeviceTimeTracker.Api.Models;
using DeviceTimeTracker.Domain.Responses.User;
using DeviceTimeTracker.Domain.Requests.User;

namespace DeviceTimeTracker.Api.Mapper
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserResponse, ReadUserModel>();

            CreateMap<WriteUserModel, PostUserRequest>();
            CreateMap<WriteUserModel, PutUserRequest>();
            CreateMap<UserResponse, ReadUserModel>();
        }
    }
}
