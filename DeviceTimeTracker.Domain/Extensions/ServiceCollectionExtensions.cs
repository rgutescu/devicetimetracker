﻿using DeviceTimeTracker.Domain.RequestHandlers;
using DeviceTimeTracker.Domain.RequestHandlers.Abstract;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DeviceTimeTracker.Domain.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddDomainDependencies(this IServiceCollection services, IConfiguration configuration)
        {
            return services
                .AddDomainServices();
        }

        private static IServiceCollection AddDomainServices(this IServiceCollection services)
        {
            return services
                .AddTransient<IQueryUserRequestHandler, QueryUserRequestHandler>()
                .AddTransient<ICommandUserRequestHandler, CommandUserRequestHandler>()
                .AddTransient<IQueryActivityLogRequestHandler, QueryActivityLogRequestHandler>()
                .AddTransient<ICommandActivityLogRequestHandler, CommandActivityLogRequestHandler>()
                .AddTransient<IQueryScheduleRequestHandler, QueryScheduleRequestHandler>()
                .AddTransient<ICommandScheduleRequestHandler, CommandScheduleRequestHandler>()
                .AddTransient<IQueryScheduleOverrideRequestHandler, QueryScheduleOverrideRequestHandler>()
                .AddTransient<ICommandScheduleOverrideRequestHandler, CommandScheduleOverrideRequestHandler>();
        }
    }
}
