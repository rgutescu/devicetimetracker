﻿namespace DeviceTimeTracker.Domain.Requests.Schedule
{
    public class PutScheduleRequest
    {
        public string UserId { get; set; }
        public ScheduleIntervalDto[] Intervals { get; set; }
    }
}
