﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using DeviceTimeTracker.Domain.RequestHandlers.Abstract;
using DeviceTimeTracker.Domain.Requests.Schedule;
using DeviceTimeTracker.Domain.Responses.Schedule;
using DeviceTimeTracker.Domain.Services.Abstract;

namespace DeviceTimeTracker.Domain.RequestHandlers
{
    public class QueryScheduleRequestHandler : IQueryScheduleRequestHandler
    {
        private readonly IScheduleService scheduleService;
        private readonly IScheduleReporterService scheduleReporterService;
        private readonly IMapper mapper;

        public QueryScheduleRequestHandler(
            IScheduleService scheduleService,
            IScheduleReporterService scheduleReporterService,
            IMapper mapper)
        {
            this.scheduleService = scheduleService;
            this.scheduleReporterService = scheduleReporterService;
            this.mapper = mapper;
        }

        public async Task<ScheduleResponse> GetScheduleByUserId(GetScheduleRequest request)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var result = await scheduleService.GetByUserIdAsync(request.UserId);

            return mapper.Map<ScheduleResponse>(result);
        }

        public async Task<UserScheduleStatusResponse> GetUserScheduleCurrentStatus(GetScheduleStatusRequest request)
        {
            if (string.IsNullOrEmpty(request?.UserId))
            {
                throw new ArgumentNullException(nameof(request));
            }

            var result = await scheduleReporterService.GetUserScheduleCurrentStatus(request.UserId);

            return mapper.Map<UserScheduleStatusResponse>(result);
        }
    }
}
