﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeviceTimeTracker.Domain.Models;
using DeviceTimeTracker.Domain.Repositories;
using DeviceTimeTracker.Domain.Services.Abstract;

namespace DeviceTimeTracker.Infrastructure.Services
{
    public class ActivityLogService : IActivityLogService
    {
        private readonly IActivityLogRepository activityLogRepository;
        private readonly IUserRepository userRepository;

        private static readonly TimeSpan MaxDelayDuration = TimeSpan.FromMinutes(2);

        public ActivityLogService(IActivityLogRepository activityLogRepository, IUserRepository userRepository)
        {
            this.activityLogRepository = activityLogRepository;
            this.userRepository = userRepository;
        }

        public async Task<IEnumerable<DetailedActivityLog>> SearchAsync(string userId, DateTime start, DateTime end)
        {
            var rawActivityLogs = await activityLogRepository.SearchAsync(userId, start, end);
            var groupedActivityLogs = rawActivityLogs.GroupBy(al => al.UserId);

            var detailedActivityLog = BuildDetailedActivityLogs(groupedActivityLogs);

            await EnrichWithUserDetails(detailedActivityLog);

            return detailedActivityLog;
        }

        private IEnumerable<DetailedActivityLog> BuildDetailedActivityLogs(IEnumerable<IGrouping<string, ActivityLog>> groupedActivityLogs)
        {
            var detailedLogEntries = new List<DetailedActivityLog>();

            foreach (var g in groupedActivityLogs)
            {
                var orderedActivities = g.OrderBy(al => al.Timestamp).ToList();

                if (orderedActivities.Count < 1)
                {
                    continue;
                }

                var startInterval = orderedActivities[0].Timestamp;
                var endInterval = orderedActivities[0].Timestamp;
                var durationSeconds = 0;

                for (var i = 1; i < orderedActivities.Count; i++)
                {
                    var currentActivity = orderedActivities[i];
                    var delayDuration = currentActivity.Timestamp - endInterval;
                    if (delayDuration > MaxDelayDuration)
                    {
                        // new interval
                        detailedLogEntries.Add(new DetailedActivityLog
                        {
                            UserId = g.Key,
                            Duration = durationSeconds / 60,
                            StartTimestamp = startInterval,
                            EndTimestamp = endInterval
                        });

                        startInterval = currentActivity.Timestamp;
                        endInterval = currentActivity.Timestamp;
                        durationSeconds = 0;
                    }
                    else // increment duration and endInterval
                    {
                        endInterval = currentActivity.Timestamp;
                        durationSeconds += (int)delayDuration.TotalSeconds;
                    }
                }

                // last interval
                detailedLogEntries.Add(new DetailedActivityLog
                {
                    UserId = g.Key,
                    Duration = durationSeconds / 60,
                    StartTimestamp = startInterval,
                    EndTimestamp = endInterval
                });
            }

            return detailedLogEntries;
        }

        private async Task EnrichWithUserDetails(IEnumerable<DetailedActivityLog> detailedActivityLog)
        {
            var userIds = detailedActivityLog.Select(al => al.UserId).Distinct();
            var userDataDictionary = new Dictionary<string, User>();
            foreach (var userId in userIds)
            {
                var userData = await userRepository.GetByIdAsync(userId);
                userDataDictionary.Add(userId, userData);
            }

            foreach (var dal in detailedActivityLog)
            {
                var userData = userDataDictionary[dal.UserId];
                dal.UserName = userData?.UserName;
                dal.Workstation = userData?.Workstation;
            }
        }

        public Task<ActivityLog> CreateAsync(ActivityLog activityLog)
        {
            return activityLogRepository.CreateAsync(activityLog);
        }
    }
}