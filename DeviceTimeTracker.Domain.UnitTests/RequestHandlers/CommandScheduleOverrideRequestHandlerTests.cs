using System;
using System.Threading.Tasks;
using AutoMapper;
using DeviceTimeTracker.Domain.Exceptions;
using DeviceTimeTracker.Domain.Models;
using DeviceTimeTracker.Domain.RequestHandlers;
using DeviceTimeTracker.Domain.Requests.ScheduleOverride;
using DeviceTimeTracker.Domain.Services.Abstract;
using Moq;
using NUnit.Framework;

namespace DeviceTimeTracker.Domain.UnitTests.RequestHandlers
{
    [TestFixture]
    public class CommandScheduleOverrideRequestHandlerTests
    {
        private CommandScheduleOverrideRequestHandler target;

        private Mock<IScheduleOverrideService> scheduleOverrideServiceMock;
        private Mock<IMapper> mapperMock;

        [SetUp]
        public void SetUp()
        {
            scheduleOverrideServiceMock = new Mock<IScheduleOverrideService>();
            mapperMock = new Mock<IMapper>();

            target = new CommandScheduleOverrideRequestHandler(scheduleOverrideServiceMock.Object, mapperMock.Object);
        }

        [Test]
        public void PutScheduleOverride_WhenNull_Throws()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => target.PutScheduleOverride(null));
        }

        [Test]
        public void PutScheduleOverride_WhenInvalid_Throws()
        {
            var request = BuildInvalidRequest();

            Assert.ThrowsAsync<InvalidModelException>(() => target.PutScheduleOverride(request));
        }

        [Test]
        public async Task PutScheduleOverride_Calls_Service()
        {
            var request = BuildValidRequest();
            _ = await target.PutScheduleOverride(request);

            scheduleOverrideServiceMock.Verify(x => x.SetAsync(It.IsAny<ScheduleOverride>()), Times.Once);
            scheduleOverrideServiceMock.VerifyNoOtherCalls();
        }

        private PutScheduleOverrideRequest BuildInvalidRequest()
        {
            return new PutScheduleOverrideRequest
            {
                Interval = new ScheduleOverrideIntervalDto
                {
                    Start = TimeSpan.FromMinutes(60),
                    End = TimeSpan.FromMinutes(50)
                }
            };
        }

        private PutScheduleOverrideRequest BuildValidRequest()
        {
            return new PutScheduleOverrideRequest
            {
                Interval = new ScheduleOverrideIntervalDto
                {
                    Start = TimeSpan.FromMinutes(60),
                    End = TimeSpan.FromMinutes(70)
                }
            };
        }
    }
}
