﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using DeviceTimeTracker.Api.Models;
using DeviceTimeTracker.Domain.RequestHandlers.Abstract;
using DeviceTimeTracker.Domain.Requests.ActivityLog;
using Microsoft.AspNetCore.Http;

namespace DeviceTimeTracker.Api.Controllers
{
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiVersion("1")]
    [ApiController]
    public class ActivityLogController : ControllerBase
    {
        private readonly IQueryActivityLogRequestHandler queryActivityLogRequestHandler;
        private readonly ICommandActivityLogRequestHandler commandActivityLogRequestHandler;
        private readonly IMapper mapper;

        public ActivityLogController(
            IQueryActivityLogRequestHandler queryActivityLogRequestHandler,
            ICommandActivityLogRequestHandler commandActivityLogRequestHandler,
            IMapper mapper)
        {
            this.queryActivityLogRequestHandler = queryActivityLogRequestHandler;
            this.commandActivityLogRequestHandler = commandActivityLogRequestHandler;
            this.mapper = mapper;
        }

        // GET: api/v1/activityLog?userId=abd&start=2021-01-01&end=2021-01-02
        [HttpGet("")]
        [ProducesResponseType(typeof(IEnumerable<ReadActivityLogModelDetails>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Search(string userId, DateTime start, DateTime end)
        {
            var request = new SearchActivityLogRequest
            {
                UserId = userId,
                Start = start.ToLocalTime().Date,
                End = end.ToLocalTime().Date
            };
            var result = await queryActivityLogRequestHandler.SearchActivities(request);

            return Ok(mapper.Map<IEnumerable<ReadActivityLogModelDetails>>(result));
        }

        [HttpPost("")]
        [ProducesResponseType(typeof(ReadActivityLogModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromBody] WriteActivityLogModel activityLog)
        {
            var request = mapper.Map<PostActivityLogRequest>(activityLog);
            
            var result = await commandActivityLogRequestHandler.PostActivity(request);

            return Ok(mapper.Map<ReadActivityLogModel>(result));
        }
    }
}
