﻿namespace DeviceTimeTracker.Domain.Requests.User
{
    public class PostUserRequest
    {
        public string UserName { get; set; }
        public string Workstation { get; set; }
        public bool Active { get; set; }
    }
}
