﻿using System.Threading.Tasks;
using DeviceTimeTracker.Domain.Requests.ActivityLog;
using DeviceTimeTracker.Domain.Responses.ActivityLog;

namespace DeviceTimeTracker.Domain.RequestHandlers.Abstract
{
    public interface ICommandActivityLogRequestHandler
    {
        Task<PostActivityLogResponse> PostActivity(PostActivityLogRequest request);
    }
}
