﻿namespace DeviceTimeTracker.Domain.Requests.User
{
    public class GetUserRequest
    {
        public string Id { get; set; }
    }
}
