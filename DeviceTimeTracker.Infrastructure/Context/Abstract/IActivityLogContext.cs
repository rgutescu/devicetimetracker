﻿using DeviceTimeTracker.Domain.Models;
using MongoDB.Driver;

namespace DeviceTimeTracker.Infrastructure.Context.Abstract
{
    public interface IActivityLogContext : IContext
    {
        IMongoCollection<ActivityLog> ActivityLogCollection { get; }
    }
}