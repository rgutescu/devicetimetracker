﻿using System;

namespace DeviceTimeTracker.Domain.Requests.ActivityLog
{
    public class SearchActivityLogRequest
    {
        public string UserId { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
    }
}
