using DeviceTimeTracker.Domain.Repositories;
using DeviceTimeTracker.Infrastructure.Services;
using Moq;
using NUnit.Framework;
using System.Threading.Tasks;
using DeviceTimeTracker.Domain.Models;

namespace DeviceTimeTracker.Infrastructure.UnitTests.Services
{
    [TestFixture]
    public class ScheduleServiceTests
    {
        private ScheduleService target;

        private Mock<IScheduleRepository> scheduleRepositoryMock;

        [SetUp]
        public void SetUp()
        {
            scheduleRepositoryMock = new Mock<IScheduleRepository>();

            target = new ScheduleService(scheduleRepositoryMock.Object);
        }

        [Test]
        public async Task GetByUserIdAsync_Calls_Repository()
        {
            var userId = "124";
            _ = await target.GetByUserIdAsync(userId);

            scheduleRepositoryMock.Verify(x => x.GetByUserIdAsync(userId), Times.Once);
            scheduleRepositoryMock.VerifyNoOtherCalls();
        }

        [Test]
        public async Task SetAsync_Calls_Repository_On_Create()
        {
            var userId = "124";
            var schedule = new Schedule() { UserId = userId};

            scheduleRepositoryMock.Setup(x => x.GetByUserIdAsync(userId)).ReturnsAsync((Schedule) null);

            _ = await target.SetAsync(schedule);

            scheduleRepositoryMock.Verify(x => x.GetByUserIdAsync(userId), Times.Once);
            scheduleRepositoryMock.Verify(x => x.CreateAsync(schedule), Times.Once);
            scheduleRepositoryMock.VerifyNoOtherCalls();
        }

        [Test]
        public async Task SetAsync_Calls_Repository_On_Update()
        {
            var userId = "124";
            var id = "1";
            var schedule = new Schedule() { UserId = userId, Id = id};

            scheduleRepositoryMock.Setup(x => x.GetByUserIdAsync(userId)).ReturnsAsync(schedule);

            _ = await target.SetAsync(schedule);

            scheduleRepositoryMock.Verify(x => x.GetByUserIdAsync(userId), Times.Once);
            scheduleRepositoryMock.Verify(x => x.UpdateAsync(It.Is<Schedule>(s => s.Id == id)), Times.Once);
            scheduleRepositoryMock.VerifyNoOtherCalls();
        }
    }
}
