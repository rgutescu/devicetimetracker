﻿using DeviceTimeTracker.Domain.Repositories;
using DeviceTimeTracker.Domain.Services.Abstract;
using DeviceTimeTracker.Infrastructure.Context;
using DeviceTimeTracker.Infrastructure.Context.Abstract;
using DeviceTimeTracker.Infrastructure.Repositories;
using DeviceTimeTracker.Infrastructure.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;

namespace DeviceTimeTracker.Infrastructure.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddInfrastructureDependencies(this IServiceCollection services, IConfiguration configuration)
        {
            return services
                .AddInfrastructureServices(configuration)
                .AddDatabaseDependencies(configuration);
        }

        private static IServiceCollection AddInfrastructureServices(this IServiceCollection services, IConfiguration configuration)
        {
            return services
                .AddTransient<IUserService, UserService>()
                .AddTransient<IActivityLogService, ActivityLogService>()
                .AddTransient<IScheduleService, ScheduleService>()
                .AddTransient<IScheduleOverrideService, ScheduleOverrideService>()
                .AddTransient<IScheduleReporterService, ScheduleReporterService>();
        }

        private static IServiceCollection AddDatabaseDependencies(this IServiceCollection services, IConfiguration configuration)
        {
            return services
                .AddSingleton<IUserRepository, UserRepository>()
                .AddSingleton<IUserContext, UserContext>()
                .AddSingleton<IActivityLogRepository, ActivityLogRepository>()
                .AddSingleton<IActivityLogContext, ActivityLogContext>()
                .AddSingleton<IScheduleRepository, ScheduleRepository>()
                .AddSingleton<IScheduleContext, ScheduleContext>()
                .AddSingleton<IScheduleOverrideRepository, ScheduleOverrideRepository>()
                .AddSingleton<IScheduleOverrideContext, ScheduleOverrideContext>();
        }
    }
}