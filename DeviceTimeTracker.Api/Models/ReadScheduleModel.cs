﻿namespace DeviceTimeTracker.Api.Models
{
    public class ReadScheduleModel : WriteScheduleModel
    {
        public string Id { get; set; }
    }
}
