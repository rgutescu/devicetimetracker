﻿namespace DeviceTimeTracker.Api.Models
{
    public class UserScheduleStatusModel
    {
        public string UserId { get; set; }
        public bool IsActive { get; set; }
        public bool IsAllowedBySchedule { get; set; }
    }
}
