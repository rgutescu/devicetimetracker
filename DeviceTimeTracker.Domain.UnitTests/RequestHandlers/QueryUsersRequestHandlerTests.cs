using System;
using System.Threading.Tasks;
using AutoMapper;
using DeviceTimeTracker.Domain.Models;
using DeviceTimeTracker.Domain.RequestHandlers;
using DeviceTimeTracker.Domain.Requests.User;
using DeviceTimeTracker.Domain.Services.Abstract;
using Moq;
using NUnit.Framework;

namespace DeviceTimeTracker.Domain.UnitTests.RequestHandlers
{
    [TestFixture]
    public class QueryUsersRequestHandlerTests
    {
        private QueryUserRequestHandler target;

        private Mock<IUserService> userServiceMock;
        private Mock<IMapper> mapperMock;

        [SetUp]
        public void SetUp()
        {
            userServiceMock = new Mock<IUserService>();
            mapperMock = new Mock<IMapper>();

            target = new QueryUserRequestHandler(userServiceMock.Object, mapperMock.Object);
        }

        [Test]
        public async Task GetUsers_Calls_Service()
        {
            _ = await target.GetUsers();

            userServiceMock.Verify(x => x.GetAsync(), Times.Once);
            userServiceMock.VerifyNoOtherCalls();
        }

        [Test]
        public void GetUserById_WhenNull_Throws()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => target.GetUserById(null));
        }

        [Test]
        public async Task GetUserById_Calls_Service()
        {
            var id = "1";
            var request = new GetUserRequest { Id = id };
            userServiceMock.Setup(x => x.GetByIdAsync(id)).ReturnsAsync(new User());

            _ = await target.GetUserById(request);

            userServiceMock.Verify(x => x.GetByIdAsync(id), Times.Once);
            userServiceMock.VerifyNoOtherCalls();
        }
    }
}
